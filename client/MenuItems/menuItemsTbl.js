import { MenuItems } from '../../imports/api/menuItems.js';
import { M } from '../lib/assets/materialize.js';
import { UserLast } from '../../imports/api/userLast.js';
import { Menus } from '../../imports/api/menu.js';

Template.menuItemsTbl.onCreated(function() {
    this.autorun( () => {
        this.subscribe("myMenuItems", Session.get("menuId"));
    });
    this.subscribe("menuItems");
    this.subscribe("userLastView");
});

Template.menuItemsTbl.onRendered(function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});

    var elemt = document.querySelectorAll('.tooltipped');
    var instancet = M.Tooltip.init(elemt, {});

    Meteor.setTimeout(function() {
        var instances = M.Modal.init(elems, {});
        var instancet = M.Tooltip.init(elemt, {});
    }, 500);
});

Template.menuItemsTbl.helpers({
    thisMenuItems: function() {
        let menuId = "";
        if (Session.get("menuId")) {
            menuId = Session.get("menuId");
        } else {
            menuId = UserLast.findOne({ view: "Menu" }).viewId;
        }
        let menuInfo = Menus.find({ _id: menuId }, { sort: { serveDateActual: 1 }});
        if (menuInfo) {
            return menuInfo
        }
    }
});

Template.menuItemsTbl.events({
    'click .deleteMenuItem' (event) {
        event.preventDefault();
        let theseIds = Session.get("menuId") + "_" + this.menuItemId;
        console.log("These Ids: " + theseIds);
        Session.set("deleteId", theseIds);
        Session.set("method", "delete.itemFromMenu");
        Session.set("item", this.menuItemName);
        Session.set("view", "Menu Items");
    },
    'click .linkToProducts' (event) {
        event.preventDefault();
        Session.set("menuItemId", this.menuItemId);
        Session.set("menuItemName", this.menuItemName);
        console.log("menu item name = " + this.menuItemName);
    },
    'click .addProdsToList' (event) {
        event.preventDefault();
        // console.log("Menu Iteme Id sent is: " + this._id);
        Session.set("menuItemId", this.menuItemId);
    }
});
