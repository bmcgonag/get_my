import { M } from '../lib/assets/materialize.js';
import { Products } from '../../imports/api/products.js';
import { MenuItems } from '../../imports/api/menuItems';
import { MenuProdLinks } from '../../imports/api/menuProdLinks.js';

Template.modalLinkProducts.onCreated(function() {
    this.subscribe("myMenuItems");
    this.subscribe("myProducts");
    this.subscribe("menuProdLinkData");
});

Template.modalLinkProducts.onRendered(function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});

    var elemse = document.querySelectorAll('select');
    var instancese = M.FormSelect.init(elemse, {
        dropdownOptions: 4,
    });
    
    setTimeout(function() {
        var instances = M.Modal.init(elems, {});
        var instancese = M.FormSelect.init(elemse, {
            dropdownOptions: 4,
        });
    }, 250);
});

Template.modalLinkProducts.helpers({
    products: function() {
        return Products.find({}, {sort: { prodName:1 }});
    }
});

Template.modalLinkProducts.events({
    'click #saveLink' (event) {
        event.preventDefault();
        let menuItemId = Session.get("menuItemId");
        let menuItemName = Session.get("menuItemName");
        let linkSelect = document.getElementById('prodForMenu');
        let linkObjArray = [];
        let links = M.FormSelect.getInstance(linkSelect).getSelectedValues();
        if (typeof links != undefined && links != [] && links != null) {
            // let's split these links into their parts, and make an array of objects
            for (i=0; i<links.length; i++) {
                let linkArray = links[i].split('|');
                let key = linkArray[0];
                let value = linkArray[1];
                let linkObj = { prodId: key, prodName: value };
                linkObjArray.push(linkObj);
            }

            Meteor.call("add.menuProdLinks", menuItemId, menuItemName, linkObjArray, function(err, result) {
                if (err) {
                    console.log("    ERROR adding product links to this menu item: " + err);
                } else {
                    Meteor.call('update.menuItemLinked', menuItemId, true, function(err, result) {
                        if (err) {
                            console.log("    ERROR adding link to menu item: " + err);
                        } else {
                            Meteor.call('link.inMenu', menuItemId, true, function(error, nresult) {
                                if (error) {
                                    console.log("   ERROR adding link to menu sub-item: " + error);
                                } else {
                                    showSnackbar("Products added to Menu Item successfully!", "green");
                                }
                            });
                        }
                    });
                }
            });
        }
    }
});
