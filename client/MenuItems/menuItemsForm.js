import { MenuItems } from '../../imports/api/menuItems.js';
import { Menus } from '../../imports/api/menu.js';
import moment from 'moment';
import { M } from '../lib/assets/materialize.js';
import { UserLast } from '../../imports/api/userLast.js';

Template.menuItemsForm.onCreated(function() {
    this.subscribe("myMenus");
    this.subscribe("allMenuItems", Session.get("menuId"));
    this.subscribe("userLastView");
});

Template.menuItemsForm.onRendered(function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {});

    Session.set("menuItemErr", false);
    Session.set("menuListItems", {});

    var elemt = document.querySelectorAll('.tooltipped');
    var instancet = M.Tooltip.init(elemt, {});

    this.autorun(() => {
        var elema = document.querySelectorAll('.autocomplete');
        var instancea = M.Autocomplete.init(elema, {
            minlength: 0,
            data: Session.get("menuListItems"),
        });
    });
});

Template.menuItemsForm.helpers({
    menuItemErr: function() {
        return Session.get("menuItemErr");
    },
    menuName: function() {
        let menuId  = "";
        if (Session.get("menuId")) {
            menuId = Session.get("menuId");
        } else {
            menuId = UserLast.findOne({ view: "Menu" }).viewId;
        }
        
        let menuInfo = Menus.findOne({ _id: menuId });
        if (menuInfo) {
            return menuInfo.menuName;
        }
    }
});

Template.menuItemsForm.events({
    'click .saveMenuItem' (event) {
        event.preventDefault();
        let menuItem = $("#menuItemInp").val();
        let dateSrv = $("#dateServed").val();
        let menuId = Session.get("menuId");


        let menuItemExists = MenuItems.findOne({ itemName: menuItem });

        if (typeof menuItemExists != 'undefined' && menuItemExists != null && menuItemExists != "") {
            // use the existing item on the menu
            let menuItemId = menuItemExists._id;
            let isLinked = menuItemExists.isLinked;
            if (menuItem == null || menuItem == "") {
                Session.set("menuItemErr", true);
            } else {
                Meteor.call('addto.Menu', menuId, menuItem, menuItemId, dateSrv, isLinked, function(error, nresult) {
                    if (error) {
                        console.log("    ERROR adding menuitem to menu: " + error);
                    } else {
                        // console.log("Added item to menu - no problem.");
                        $("#menuItemInp").val("");
                        $("#dateServed").val("");
                    }
                });
            }
        } else {
            // add the item as new and add to the menu
            if (menuItem == null || menuItem == "") {
                Session.set("menuItemErr", true);
            } else {
                Meteor.call('add.menuItem', menuItem, function(err, result) {
                    if (err) {
                        console.log("    ERROR adding menu item: " + err);
                    } else {
                        // console.log("    SUCCESS adding menu item.");
                        $("#menuItemInp").val("");
                        $("#dateServed").val("");

                        // now add this item to the menu
                        Meteor.call('addto.Menu', menuId, menuItem, result, dateSrv, false, function(error, nresult) {
                            if (error) {
                                console.log("    ERROR adding menuitem to menu: " + error);
                            } else {
                                // console.log("Added item to menu - no problem.");
                            }
                        });
                    }
                });
            }
        }
    },
    'click .shiftOneDay' (event) {
        event.preventDefault();
        let menuInfo = MenuItems.find({}).fetch();
        // now menuInfo is an array
        let menuInfoLen = menuInfo.length;
        for (i = 0; i < menuInfoLen; i++) {
            let menuItemId = menuInfo[i]._id;
            let momentAddDay = moment(menuInfo[i].serveDate).add(1, 'day').format("MMM DD, YYYY");
            // console.log(momentAddDay);
            Meteor.call('shiftDate', menuItemId, momentAddDay, function(err,result) {
                if (err) {
                    console.log("    ERROR shifting meal days: " + err);
                } else {
                    showSnackbar("Items Shifted Out by 1 Calendar Day", "green");
                }
            });
        }
    },
    'keyup #menuItemInp' (event) {
        if (event.which != 13) {
            let findMenuItem = $("#menuItemInp").val();
            let menuItemInfo = MenuItems.find({ itemName: {$regex: findMenuItem + '.*', $options: 'i' }}).fetch();
            if (typeof menuItemInfo != 'undefined' && menuItemInfo != '' && menuItemInfo != null) {
                getMenuItemList(menuItemInfo);
            }
        }
    },
});

getMenuItemList = function(menuItemInfo) {
    let menuItemObjArray = [];
    menuItemObjArray = menuItemInfo.map(info => ({ id: info._id, text: info.itemName }));
    Session.set("menuListItems", menuItemObjArray);
}
