import { M } from '../lib/assets/materialize.js';
import { MenuProdLinks } from '../../imports/api/menuProdLinks';
import { Products } from '../../imports/api/products.js';
import { Lists } from '../../imports/api/lists.js';
import { ListItems } from '../../imports/api/listItems';

Template.addProdToListModal.onCreated(function() {
    this.subscribe("myProducts");
    this.subscribe("myLists");
    this.subscribe("myListItems");
    this.subscribe("menuProdLinkData");
});

Template.addProdToListModal.onRendered(function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});

    Session.set("itemsSelected", []);
});

Template.addProdToListModal.helpers({
    assocProds: function() {
        let menuItemId = Session.get("menuItemId");
        let assocProds = MenuProdLinks.find({ menuItemId: menuItemId });
        if (typeof assocProds != 'undefined' && assocProds != '' && assocProds != null) {
            return assocProds;
        }
    },
    setOfLists: function() {
        return Lists.find({});
    },
    productToChoose: function() {
        let menuItemId = Session.get("menuItemId");
        console.log("Menu Item Id received is: " + menuItemId);
        let prodLinkLIst = MenuProdLinks.find({ menuItemId: menuItemId});
        console.dir(prodLinkLIst);
        if (typeof prodLinkLIst != 'undefined' && prodLinkLIst != "" && prodLinkLIst != null) {
            return prodLinkLIst;
        }
    },
});

Template.addProdToListModal.events({
    'click .productListing' (event) {
        let itemId = event.currentTarget.id;
        let selected = Session.get("itemsSelected");
        console.log("Item clicked: " + itemId);
        selected.push(itemId);
        console.dir(selected);
        Session.set("itemsSelected", selected);
    },
    'click #saveProdsToList' (event) {
        event.preventDefault();
        let selectedItems = Session.get("itemsSelected");
        let listId = $("#chooseList").val();
        Meteor.call('add.itemsFromMenuItem', selectedItems, listId, function(err, result) {
            if (err) {
                console.log("    ERROR adding menu components to list: " + err);
            } else {
                showSnackbar("Items Added to List!", "green");
            }
        });
    }
});
