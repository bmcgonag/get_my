import { TaskItems } from '../../imports/api/tasks.js';
import { M } from '../lib/assets/materialize.js';

Template.myTasksForm.onCreated(function() {
    
});

Template.myTasksForm.onRendered(function() {  
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {});

    var elemsd = document.querySelectorAll('.chips');
    var instancesd = M.Chips.init(elemsd, {});

    Session.set("hideComplete", true);
    Session.set("onlyToday", true);
    Session.set("taskDateArr", []);
});

Template.myTasksForm.helpers({
    taskDates: function() {
        return Session.get("taskDateArr");
    },
});

Template.myTasksForm.events({
    'click .addNewTask' (event) {
        event.preventDefault();
        let taskNameErr = false;
        let taskDateErr = false;
        let elemsc = document.getElementById('taskName');
        let taskNameArray = M.Chips.getInstance(elemsc).chipsData;
        let taskDateArray = Session.get("taskDateArr");
        let actDate = [];

        if (taskNameArray == null || taskNameArray == [] || taskNameArray == "") {
            taskNameErr = true;
        }

        if (taskDateArray == null || taskDateArray == []|| taskDateArray == "") {
            taskDateErr = true;
        } else {
            for (i = 0; i < taskDateArray.length; i++) {
                // console.log(taskDateArray[i]);
                let actDateTask = new Date(taskDateArray[i]);
                actDate.push(actDateTask);
            }
        }

        console.log("Date Error: " + taskDateErr + " - Name Error: " + taskNameErr);
        if (taskDateErr == false && taskNameErr == false) {
            Meteor.call("add.task", taskNameArray, "self", "selfId", taskDateArray, actDate, function(err, result) {
                if (err) {
                    console.log("    ERROR adding task for self: " + err);
                    showSnackbar("ERROR adding task for self!", "red");
                } else {
                    console.log("    SUCCESS adding task for self.");
                    Session.set("taskDateArr", []);
                    $("#myTaskName").val("");
                    $("#myTaskDate").val("");
                    showSnackbar("Added Tasks Successfully!", "green");
                }
            });
        } else {
            showSnackbar("Error! Both Task & Date are Required!", "red");
        }
        
    },
    'click #showMyCompletedTasks' (event) {
        let hide = $("#showMyCompletedTasks").prop('checked');
        if (hide == true) {
            Session.set("hideComplete", false);
            Session.set("onlyToday", false);
            $("#showAllTasks").prop('checked', true);
        } else {
            Session.set("hideComplete", true);
        }
    },
    'click #showAllTasks' (event) {
        let onlyToday = $("#showAllTasks").prop('checked');
        if (onlyToday == true) {
            Session.set("onlyToday", false);
        } else {
            Session.set("onlyToday", true);
        }

    },
    'change #myTaskDate' (event) {
        let taskDate = $("#myTaskDate").val();
        let taskDateArr = Session.get("taskDateArr");
        taskDateArr.push(taskDate);
        Session.set("taskDateArr", taskDateArr);
    },
});