import { UserConfig } from "../imports/api/userConfig";

Template.MainLayout.onCreated(function() {
    this.subscribe("UserConfigPrefs");
});

Template.MainLayout.onRendered(function() {
    this.autorun(() => {
        let myId = Meteor.userId();
        let myprefs = UserConfig.findOne({ user: myId });
        if (typeof myprefs != 'undefined') {
            if (myprefs.darkMode == "light") {
                console.log("Found theme as light");
                Session.set("myTheme", "light");
                document.documentElement.setAttribute('theme', "light");
            } else {
                console.log("Found theme as dark");
                Session.set("myTheme", "dark");
                document.documentElement.setAttribute('theme', "dark");
            } 
        } else {
            console.log("User Prefs appear undefined.");
        }
    });
});

Template.MainLayout.helpers({
    
});

Template.MainLayout.events({

});