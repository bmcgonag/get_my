import { UserConfig } from "../../imports/api/userConfig";

Template.userConfig.onCreated(function() {

});

Template.userConfig.onRendered(function() {
    let myConfig = UserConfig.findOne({ user: Meteor.userId() });
    if (typeof myConfig != 'undefined') {
        console.log("My Pref: " + myConfig.darkPref);
        if (myConfig.darkMode == 'light') {
            $("#darkMode").prop('checked', false);
        } else {
            $("#darkMode").prop('checked', true);
        }
    }
});

Template.userConfig.helpers({

});

Template.userConfig.events({
    'click #darkMode' (event) {
        let darkModePref = $("#darkMode").prop('checked');
        if (darkModePref == true) {
            Meteor.call('update.darkModePref', 'dark', function(err, reuslt) {
                if (err) {
                    console.log("    ERROR: could not set dark mode preference to dark: " + err);
                } else {
                    showSnackbar("Dark Mode Preference Set to Dark", "green");
                }
            });
        } else {
            Meteor.call('update.darkModePref', 'light', function(err, result) {
                if (err) {
                    console.log("    ERROR: could not set dark mode preference to light: " + err);
                } else {
                    showSnackbar("Dark Mode Preference Set to Light", "green");
                }
            });
        }
    },
});