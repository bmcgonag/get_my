import { ListItems } from '../../imports/api/listItems.js'
import { Lists } from '../../imports/api/lists.js';
import { Products } from '../../imports/api/products.js';
import { M } from '../lib/assets/materialize.js';
import { UserLast } from '../../imports/api/userLast.js';

Template.listItemsForm.onCreated(function() {
    this.subscribe("myListItems", Session.get("listId"));
    this.subscribe("myLists");
    this.subscribe("myProducts");
    this.subscribe("userLastView");
});

Template.listItemsForm.onRendered(function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
    Session.set("listItemEditMode", false);
    Session.set("itemReqErr", false);
    Session.set("showReceivedItems", false);
    Session.set("filtering", false);
    Session.set("findListItems", {});
    this.autorun(() => {
        var elems = document.querySelectorAll('.autocomplete');
        var instances = M.Autocomplete.init(elems, {
            minLength: 0,
            data: Session.get("findListItems"),
        });
    });
});

Template.listItemsForm.helpers({
    selListName: function() {
        let selListId = "";
        if (Session.get("listId")) {
            selListId = Session.get("listId");
        } else {
            selListInfo = UserLast.findOne({ view: "List" });
            if (selListInfo) {
                selListId = selListInfo.viewId;
                Session.set("listId", selListId);
            } else {
                console.log("not finding any value for viewId.");
            }
        }
        let listInfo = Lists.findOne({ _id: selListId });
        if (listInfo) {
            return listInfo.listName;
        }
    },
    itemProdName: function() {
        return Products.find({});
    },
    editMode: function() {
        return Session.get("listItemEditMode");
    },
    filtering: function() {
        return Session.get("filtering");
    }
});

Template.listItemsForm.events({
    'click .saveListItem' (event) {
        event.preventDefault();
        let item = $("#findListItems").val();
        let listId = Session.get("listId");
        if (item == null || item == "") {
            Session.set("itemReqErr", true);
        } else {
            Meteor.call("add.listItem", item, listId, function(err, result) {
                if (err) {
                    console.log("    ERROR adding item to list: " + err);
                } else {
                    console.log("    SUCCESS adding item to list.");
                    $("#findListItems").val("");
                }
            });
        }
    },
    'keydown #findListItems' (event) {
        if (event.which === 13) {
            let item = $("#findListItems").val();
            let listId = Session.get("listId");
            if (item == null || item == "") {
                Session.set("itemReqErr", true);
            } else {
                Meteor.call("add.listItem", item, listId, function(err, result) {
                    if (err) {
                        console.log("    ERROR adding item to list: " + err);
                    } else {
                        console.log("    SUCCESS adding item to list.");
                        $("#findListItems").val("");
                    }
                });
            }
        }
    },
    'click #showReceivedItems' (event) {
        if ($("#showReceivedItems").prop('checked') == true) {
            Session.set("showReceivedItems", true);
        } else {
            Session.set("showReceivedItems", false);
        }
    },
    'keyup #searchListItems' (event) {
        if (event.which !== 13) {
            let searchVal = $("#searchListItems").val();
            Session.set("searchVal", searchVal);
        }
    },
    'keyup #findListItems' (event) {
        if (event.which !== 13) {
            let findItemVal = $("#findListItems").val();
            let listItemInfo = Products.find({ prodName: {$regex: findItemVal + '.*', $options: 'i'}}).fetch();
            if (typeof listItemInfo != 'undefined' && listItemInfo != "" && listItemInfo != null) {
                getDataList(listItemInfo);
            }
        }
    },
    'click #filterOn' (event) {
        Session.set("filtering", true);
    },
    'click #filterOff' (event) {
        // clear filter field
        Session.set("searchVal", "");
        $("#searchListItems").val("");
        Session.set("filtering", false);
    }
});

getDataList = function(listItemInfo) {
    let listItemObjArray = [];
    listItemObjArray = listItemInfo.map(info => ({ id: info._id, text: info.prodName, store: info.prodStore }))
    Session.set("findListItems", listItemObjArray);
}
