import { ListItems } from '../../imports/api/listItems.js';
import { M } from '../lib/assets/materialize.js';
import { UserLast } from '../../imports/api/userLast.js';
import { Stores } from '../../imports/api/stores.js';

Template.listItemTblByStore.onCreated(function() {
    this.autorun( () => {
        this.subscribe("myListItems", Session.get("listId"));
    });
    this.subscribe("storeInfo");
    this.subscribe("userLastView");
});

Template.listItemTblByStore.onRendered(function() {
    Session.set("showReceivedItems", false);
    Session.set("searchVal", "");
});

Template.listItemTblByStore.helpers({
    stores: function() {
        let storeList = Stores.find({});
        return storeList;
    },
    thisListItems: function() {
        let showReceived = Session.get("showReceivedItems");
        let searchVal = Session.get("searchVal");
        let store = this.storeName;
        if (showReceived == false) {
            if (typeof searchVal == 'undefined' || searchVal.length === 0) {
                return ListItems.find({ itemStore: store, itemReceived: false });
            } else {
                return ListItems.find({ itemStore: store, itemReceived: false, itemName: { $regex: searchVal + '.*', $options: 'i' }})
            }
        } else {
            if (typeof searchVal == 'undefined' || searchVal.length == 0) {
                return ListItems.find({ itemStore: store });
            } else {
                return ListItems.find({ itemStore: store, itemName: { $regex: searchVal + '.*', $options: 'i' } });
            }
        }
    },
    storeInList: function() {
        let noStoresInList = ListItems.find({ itemStore: this.storeName }).count();
        if (noStoresInList > 0) {
            return true;
        } else {
            return false;
        }
    },
});

Template.listItemTblByStore.events({
    'click li' (event) {
        let itemSel = (event.currentTarget.id).split('-');
        let itemId = itemSel[0];
        let itemOrder = itemSel[1];

        if (typeof itemOrder == 'undefined' || itemOrder == "" || itemOrder == false) {
            Meteor.call("setOrdered.listItem", itemId, function(err, result) {
                if (err) {
                    console.log("    ERROR updating order status to ordered: " + err);
                }
            });
        } else {
            Meteor.call("setNotOrdered.listItem", itemId, function(err, result) {
                if (err) {
                    console.log("    ERROR updating order status to unordered: " + err);
                }
            });
        }
    },
    'click .markListItemReceived' (event) {
        event.preventDefault();
        Meteor.call('setReceived.listItem', this._id, function(err, result) {
            if (err) {
                console.log("    ERROR setting item as received: " + err);
            } else {
                // console.log("    SUCCESS setting item received.");
            }
        });
    },
    'click .deleteListItem' (event) {
        event.preventDefault();
        Session.set("deleteId", this._id);
        Session.set("method", "delete.listItem");
        Session.set("item", this.itemName);
        Session.set("view", "List Items");
    },
});
