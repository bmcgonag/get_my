import { Lists } from "../../imports/api/lists";
import { Products } from "../../imports/api/products";
import { Stores } from "../../imports/api/stores";
import { Menus } from '../../imports/api/menu.js';
import { MenuItems } from '../../imports/api/menuItems.js';
import moment from 'moment';
import { TaskItems } from "../../imports/api/tasks";
import { UpdateInfo } from '../../imports/api/updateInfo.js';
import { SysConfig } from '../../imports/api/systemConfig.js';

Template.dashboard.onCreated(function() {
    this.subscribe("userList");
    this.subscribe("myLists");
    this.subscribe("myCategories");
    this.subscribe("storeInfo");
    this.subscribe("myProducts");
    this.subscribe("myLocations");
    // this.subscribe("myMenus");
    this.subscribe("todayMenuItems");
    this.subscribe("myTasks");
    this.subscribe("UpdateVersion");
    this.subscribe("SystemConfig");
});

Template.dashboard.onRendered(function() {
    
});

Template.dashboard.helpers({
    userCount: function() {
        return Meteor.users.find().count();
    },
    listCount: function() {
        return Lists.find().count();
    },
    storeCount: function() {
        return Stores.find().count();
    },
    productCount: function() {
        return Products.find().count();
    },
    catCount: function() {
        return Categories.find().count();
    },
    locCount: function() {
        return Locations.find().count();
    },
    mytaskitems: function() {
        let today = moment(new Date()).format("MMM DD, YYYY");
        return TaskItems.find({ isComplete: false, taskDate: today });
    },
    todayMenuItem: function() {
        let myMenus = Menus.find({}).fetch();
        console.dir(myMenus);
        return myMenus;
    },
    todayDate: function() {
        let now = new Date();
        let todayDate = moment(now).format("MMM DD, YYYY");
        return todayDate;
    },
    updates: function() {
        let updateAvail = UpdateInfo.find({});
        return updateAvail;
    },
    updatesExist: function() {
        let updateExists = UpdateInfo.find({ viewed: false }).fetch();
        if (updateExists.length > 0) {
            return true;
        } else {
            return false;
        }
    },
    currConfig: function() {
        return SysConfig.findOne({});
    },
    descriptionSinHTML: function() {
        let desc = this.description;
        let sinH = $(desc).text();
        return desc;
    },
    niceDate: function() {
        let rDateNorm = this.date;
        let rDate = moment(rDateNorm).format('LL');
        return rDate;
    }
});

Template.dashboard.events({
    "click .cardLink" (event) {
        let link = event.currentTarget.id;
        switch(link) {
            case "userMgmtLink":
                FlowRouter.go('/userMgmt');
                break;
            case "listMgmtLink":
                FlowRouter.go('/manageLists');
                break;
            case "storeMgmtLink":
                FlowRouter.go('/manageStore');
                break;
            case "prodMgmtLink":
                FlowRouter.go('/manageProduct');
                break;
            case "myMenuLink":
                FlowRouter.go('/mymenus');
                break;
            case "taskMgmtLink":
                FlowRouter.go('/taskHome');
                break;
            default:
                break;
        }
    },
    'click .card' (event) {
        let cardId = event.currentTarget.id;

        switch(cardId) {
            case "userInfoCard":
                FlowRouter.go('/userMgmt');
                break;
            case "listInfoCard":
                FlowRouter.go("/mylists");
                break;
            case "storeInfoCard":
                FlowRouter.go('/manageStore');
                break;
            case "prodInfoCard":
                FlowRouter.go("/manageProduct");
                break;
            case "menuInfoCard":
                FlowRouter.go('/mymenus');
                break;
            case "taskInfoCard":
                FlowRouter.go('/myTasks');
                break;
            default:
                break;
        }
    },
    'click .readLink' (event) {
        let eventId = event.currentTarget.id;

        Meteor.call('markUpdate.read', eventId, function(err, result) {
            if (err) {
               console.log("    ERROR marking update as 'read': " + err);
            } else {
                console.log("marked read successfully!");
            }
        });
    }
});
