import { Lists } from '../../imports/api/lists.js';
import { M } from '../lib/assets/materialize.js';

Template.listsTbl.onCreated(function() {
    this.subscribe("myLists");
});

Template.listsTbl.onRendered(function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});
});

Template.listsTbl.helpers({
    mylists: function() {
        return Lists.find({});
    },
});

Template.listsTbl.events({
    'click li.collection-item' (event) {
        event.preventDefault();
        let sender = event.target;
        if (sender.localName == "li" || sender.localName == "span") {
            let listId = event.currentTarget.id;
            if (listId == "addList") {
                // opens the modal and allows you to add a new List
            } else {
                // console.log("listId is: " + listId);
                Session.set("listId", listId);
                Meteor.call('add.userLast', "List", listId, function(err, result) {
                    if (err) {
                        console.log("    ERROR setting user last list id in db: " + err);
                    } else {
                        Meteor.setTimeout(function() {
                            FlowRouter.go('/listitems');
                        }, 100); 
                    }
                });
            }
        }
    },
    'click i.markAsComplete' (event) {
        event.preventDefault();
        let sender = event.target;
        if (sender.localName == "i") {
            let listFullId = event.currentTarget.id;
            let splitList = listFullId.split("_");
            let listId = splitList[1];
            // console.log("listId is " + listId);
            Meteor.call("mark.complete", listId, function(err, result){
                if (err) {
                    console.log("    ERROR marking list complete! " + err);
                    showSnackbar("ERROR! List Not Makred Complete!", "red");
                } else {
                    // console.log("    SUCCESS marking list complete.");
                    showSnackbar("List Marked Complete!", "green");
                }
            });
        }
    }
});