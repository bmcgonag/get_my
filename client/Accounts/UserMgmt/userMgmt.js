import { M } from '../../lib/assets/materialize.js';

Template.userMgmt.onCreated(function() {
    this.subscribe("userList");
});

Template.userMgmt.onRendered(function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});

    var elemm = document.querySelectorAll('.modal');
    var instancem = M.Modal.init(elemm, {});;
});

Template.userMgmt.helpers({
    userInfo: function() {
        return Meteor.users.find({});
    },
    userName: function() {
        return this.profile.fullname;
    },
    userEmail: function() {
        return this.emails[0].address;
    },
    userRole: function() {
        return Roles.getRolesForUser( this._id );
    }
});

Template.userMgmt.events({
    "click .editUser" (event) {
        event.preventDefault();
        let userId = this._id;
        Session.set("usersId", userId);
    },
    "click .deleteUser" (event) {
        event.preventDefault();
        let userId = this._id;
        console.log("Delete called on : " + userId);
        Session.set("deleteId", userId);
        Session.set("item", "User");
        Session.set("view", "Users");
        Session.set("method", "delete.userFromSys");
    }
});