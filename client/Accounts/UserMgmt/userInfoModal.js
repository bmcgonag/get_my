import { M } from '../../lib/assets/materialize.js';

Template.userInfoModal.onCreated(function() {
    this.subscribe("rolesAvailable");
});

Template.userInfoModal.onRendered(function() {
    Session.set("passMatch", true);
    Session.set("passErr", false);
    Session.set("userEmailErr", false);
    Session.set("userRoleErr", false);

    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});

    var elemse = document.querySelectorAll('select');
    var instancese = M.FormSelect.init(elemse, {});

    Meteor.setTimeout(() => {
        instances = M.Modal.init(elems, {});
        instancese = M.FormSelect.init(elemse, {});
    }, 500);
});

Template.userInfoModal.helpers({
    passMatch: function() {
        return Session.get("passMatch");
    },
    emailEmpty: function() {
        return Session.get("userEmailErr");
    },
    roleEmpty: function() {
        return Session.get("userRoleErr");
    },
    userInfo: function() {
        let usersId = Session.get("usersId");
        if (usersId != "" && usersId != null) {
            let usersInfo = Meteor.users.findOne({ _id: usersId });
            // console.dir(usersInfo);
            Session.set("usersInfo", usersInfo);
            return usersInfo;
        } else {
            return;
        }
    },
    userRole: function() {
        let userRole = Roles.getRolesForUser( Session.get("usersId"));
        Session.set("usersRole", userRole);
        console.log(userRole);
        return userRole;
    },
    rolesOptions: function() {
        return Roles.find();
    }
});

Template.userInfoModal.events({
    "click #saveChanges" (event) {
        event.preventDefault();
        let usersId = Session.get("usersId");
        let passwd = $("#newPass").val();
        let usersEmail = $("#usersEmail").val();
        let userRole = $("#userRole").val();
        let userInfo = Session.get("usersInfo");

        let currEmail = userInfo.emails[0].address;
        let userDbRole = Session.get("usersRole");

        let currRole = userDbRole[0];

        let passMatch = Session.get("passMatch");
        if (passMatch == false) {
            Session.set("passErr", true);
            return;
        } else {
            Session.set("passErr", false);
        }
        
        if (usersEmail == null || usersEmail == "") {
            Session.set("userEmailErr", true);
            return;
        } else {
            Session.set("userEmailErr", false);
        }
        
        if (userRole == null || userRole == "") {
            Session.set("userRoleErr", true);
            return;
        } else {
            Session.set("userRoleErr", false);
        }
        
        if (passMatch == true || passMatch == "NA") {
            if (passwd != "" && passwd != null) {
                // need to account for the admin changing passwords and userRole or Email
                changePassword(usersId, passwd);
            }
            
            if (usersEmail != null && usersEmail != "" && usersEmail != currEmail) {
                changeUserEmail(usersId, usersEmail);
            }

            if (userRole != null && userRole != "" && userRole != currRole) {
                changeUserRole(usersId, userRole);
            }
        }
    },
    "click #closePass" (event) {;

    },
    "keyup #newPassConf" (event) {
        let newPass = $("#newPass").val();
        let newPassConf = $("#newPassConf").val();
        if (newPassConf.length > 2 && (newPass != null || newPass != "")) {
            if (newPass != newPassConf) {
                Session.set("passMatch", false);
            } else {
                Session.set("passMatch", true);
            }
        }
    }
});

changePassword = function(userId, passwd) {
    console.log("would change password.");
    Meteor.call('edit.userPass', userId, passwd, function(err, result) {
        if (err) {
            console.log("    ERROR changing user passwrod:" + err);
        } else {
            showSnackbar("Successfully Saved Changes!", "green");
            console.log("    Password changed successfully!");
        }
    });
}

changeUserEmail = function(usersId, usersEmail) {
    console.log("Would change user email");
    Meteor.call('update.userEmail', usersId, usersEmail, function(err, result) {
        if (err) {
            console.log("    ERROR updating user email: " + err);
        } else {
            showSnackbar("Email updated successfully!", "green");
        }
    });
}

changeUserRole = function(userId, role) {
    console.log("Would change user Role.");
    Meteor.call('edit.userRole', userId, role, function(err, result) {
        if (err) {
            console.log("    ERROR updating user role: " + err);
        } else {
            showSnackbar("Role Successfully Updated!", "green");
        }
    });
}