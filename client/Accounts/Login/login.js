import { SysConfig } from '../../../imports/api/systemConfig.js';

Template.login.onCreated(function() {
    this.subscribe("SystemConfig");
});

Template.login.onRendered(function() {
    
});

Template.login.helpers({
    areFilled: function() {
        return Session.get("filledFields");
    },
    canReg: function() {
        let conf = SysConfig.findOne();
        if (typeof conf != 'undefined') {
            return conf.allowReg;
        } else {
            return true;
        }
    }
});

Template.login.events({
    'click #logmein' (event) {
        event.preventDefault();
        console.log("clicked login");
        let email = $("#email").val();
        let pass = $("#password").val();

        if (email == null || email == "" || pass == "" || pass == null) {
            Session.set("filledFields", false);
            return;
        } else {
            Meteor.loginWithPassword(email, pass);
        }
    },
    'click #reg' (event) {
        event.preventDefault();
        FlowRouter.go('/reg');
    },
});