import { M } from '../lib/assets/materialize';
import { UpdateInfo } from '../../imports/api/updateInfo.js';

Template.headerBar.onCreated(function() {
    this.subscribe("UpdateVersion");
});

Template.headerBar.onRendered(function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {});
    var elemd = document.querySelectorAll('.dropdown-trigger');
  var instances = M.Dropdown.init(elemd, {});
});

Template.headerBar.helpers({
    adminReg: function() {
        return Session.get("adminreg");
    },
    myTheme: function() {
        return Session.get("myTheme");
    },
    updateExists: function() {
        let update = UpdateInfo.find({ viewed: false }).fetch();
        if (update.length > 0) {
            return true;
        } else {
            return false;
        }
    },
});

Template.headerBar.events({
	'click  .navBtn' (event) {
        event.preventDefault();
        var clickedTarget = event.target.id;
        // console.log("clicked " + clickedTarget);
        if (clickedTarget == 'mainMenu') {
            FlowRouter.go('/');
        } else {
            // console.log("should be going to /" + clickedTarget);
            FlowRouter.go('/' + clickedTarget);
        }
    },
    'click .signOut': () => {
        FlowRouter.go('/');
        Meteor.logout();
    },
    'click #brandLogo' (event) {
        event.preventDefault();
        FlowRouter.go('/dashboard');
    }
});
