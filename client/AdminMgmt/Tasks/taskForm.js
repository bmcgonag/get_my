import { Tasks } from '../../../imports/api/tasks.js';
import { M } from '../../lib/assets/materialize.js';

Template.taskForm.onCreated(function() {
    this.subscribe("userList");
    this.subscribe("allTasks");
});

Template.taskForm.onRendered(function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
    
    var elemd = document.querySelectorAll('.datepicker');
    var instanced = M.Datepicker.init(elemd, {});

    var elemc = document.querySelectorAll('.chips');
    var instancec = M.Chips.init(elemc, {
      placeholder: 'Task Name',
      secondaryPlaceholder: '+Task Name',
    });

    setTimeout(function() {
        instances = M.FormSelect.init(elems, {});
    }, 350);
    
    Session.set("taskNameErr", false);
    Session.set("taskUserErr", false);
    Session.set("taskDateErr", false);
    Session.set("hideCompletedTasks", true);
    Session.set("taskDateArr", []);
});

Template.taskForm.helpers({
    taskUsers: function() {
        return Meteor.users.find({});
    },
    username: function() {
        return this.profile.fullname;
    },
    usersId: function() {
        return this._id;
    },
    taskDates: function() {
        return Session.get("taskDateArr");
    },
});

Template.taskForm.events({
    'click #testChips' (event) {
        event.preventDefault();
        let elemcc = document.getElementById('taskName');
        let taskName = M.Chips.getInstance(elemcc).chipsData;
        console.log(taskName);
    },
    'change #taskDate' (event) {
        let taskDate = $("#taskDate").val();
        let taskDateArr = Session.get("taskDateArr");
        taskDateArr.push(taskDate);
        Session.set("taskDateArr", taskDateArr);
    },
    'click .saveTaskMgmt' (event) {
        event.preventDefault();
        let elemcc = document.getElementById('taskName');
        let taskNameArr = M.Chips.getInstance(elemcc).chipsData;
        let taskDateArr = Session.get("taskDateArr");
        let taskUser = $("#taskUser").val();
        let taskUserErr = false;
        let taskNameErr = false;
        let taskDateErr = false;
        let userInfo;
        let actDate = [];
        // console.dir(taskNameArr);

        if (taskNameArr == null || taskNameArr == []) {
            taskNameErr = true;
        }

        if (taskDateArr == null || taskDateArr == []) {
            taskDateErr = true;
        } else {
            for (i = 0; i < taskDateArr.length; i++) {
                // console.log(taskDateArr[i]);
                let actDateTask = new Date(taskDateArr[i]);
                actDate.push(actDateTask);
            }
        }

        if (taskUser == null || taskUser == "") {;
            taskUserErr = true;
        } else {
            userInfo = taskUser.split('_');
        }

        if (taskUserErr == false && taskDateErr == false && taskNameErr == false) {
            Meteor.call("add.task", taskNameArr, userInfo[0], userInfo[1], taskDateArr, actDate, function(err, result) {
                if (err) {
                    console.log("    ERROR adding the new task: " + err);
                } else {
                    console.log("    SUCCESS adding the new task.");
                    Session.set("taskDateArr", []);
                    $("#taskDate").val("");
                    $("#taskUser").val("");
                    $('select').formSelect();
                }
            });
        } else {
            showSnackbar("ERROR: Missing Required Fields!", "red");
        }
    },
    'click #hideCompletedTasks' (event) {
        let hideComp = $("#hideCompletedTasks").prop('checked');
        if (hideComp == true) {
            Session.set("hideCompletedTasks", true);
        } else {
            Session.set("hideCompletedTasks", false);
        }
        
    }
});