import { TaskItems } from '../../../imports/api/tasks.js';
import moment from 'moment';
import { M } from '../../lib/assets/materialize.js';

Template.taskTbl.onCreated(function() {
    this.subscribe("allTasks");
});

Template.taskTbl.onRendered(function() {
    var elem = document.querySelectorAll('.modal');
    var instance = M.Modal.init(elem, {});

    Session.set("hideCompletedTasks", false);
});

Template.taskTbl.helpers({
    tasks: function() {
        let hideComp = Session.get("hideCompletedTasks");
        if (hideComp == false) {
            return TaskItems.find({}, { sort: { actualDate: 1 }});
        } else {
            return TaskItems.find({ isComplete: false }, { sort: { actualDate: 1 }});
        }
    },
    completeDate: function() {
        let completedOn = this.completedOn;
        if (completedOn != null) {
            let compOn = moment(completedOn).format("MMM DD, YYYY");
            return compOn;
        }
    }
});

Template.taskTbl.events({
    'click .deleteTask' (event) {
        event.preventDefault();
        Session.set("deleteId", this._id);
        Session.set("method", "delete.task");
        Session.set("item", this.taskName);
        Session.set("view", "Tasks");
        // $('#modalDelete').modal('open');
    },
    'click .markTaskComplete' (event) {
        event.preventDefault();
        let taskId = this._id;
        Meteor.call("markTask.complete", taskId, function(err, result) {
            if (err) {
                console.log("    ERROR marking task completeL " + err);
                showSnackbar("ERROR Marking Task Complete!", "red");
            } else {
                showSnackbar("Successfully Marked Task Complete!", "green");
            }
        });
    },
});