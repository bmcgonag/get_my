import { Products } from '../../../imports/api/products.js';
import { M } from '../../lib/assets/materialize.js';

Template.prodMgmtTbl.onCreated(function() {
    this.subscribe("myProducts");
});

Template.prodMgmtTbl.onRendered(function() {
    Session.set("searchProds", false);
    Session.set("searchStore", false);
    Session.set("noStoreSet", false);
});

Template.prodMgmtTbl.helpers({
    products: function() {
        let searchProd = Session.get("searchProds");
        let searchStore = Session.get("searchStore");
        let noStoreSet = Session.get("noStoreSet");

        if (searchProd == true) {
            let searchVal = Session.get("searchVal");
            if (typeof searchVal == 'undefined' || searchVal.length == 0) {
                return Products.find({});
            } else {
                return Products.find({ prodName: { $regex: searchVal + '.*', $options: 'i' } }, { sort: { prodName: 1 }});
            }
        } else if (searchStore == true) {
            let searchVal = Session.get("searchStoreVal");
            if (typeof searchVal == 'undefined' || searchVal.length == 0) {
                return Products.find({});
            } else {
                return Products.find({ prodStore: { $regex: searchVal + '.*', $options: 'i' } }, { sort: { prodName: 1 }});
            }
        } else if (noStoreSet == true) {
            return Products.find({ prodStore: '' }, { sort: { prodName: 1 }});
        } else {
            return Products.find({}, { sort: { prodName: 1 }});
        }
    },
    searchProd: function() {
        return Session.get("searchProds");
    },
    searchStore: function() {
        return Session.get("searchStore");
    },
});

Template.prodMgmtTbl.events({
    'click .deleteProduct' (event) {
        event.preventDefault();
        Session.set("deleteId", this._id);
        Session.set("method", "delete.product");
        Session.set("item", this.prodName);
        Session.set("view", "Products");;
    },
    'click .editProduct' (event) {
        event.preventDefault();
        Session.set("prodEditMode", true);
        Session.set("prodEditId", this._id);
        let prodInfo = Products.findOne({ _id: this._id });
        $("#prodName").val(prodInfo.prodName);
        $("#prodStore").val(prodInfo.prodStore);
        // $('select').formSelect();
    },
    'click #filterProds' (event) {
        event.preventDefault();
        Session.set("searchProds", true);
    },
    'click #closeFilter' (event) {
        event.preventDefault();
        Session.set("searchProds", false);
    },
    "keyup #searchProds" (event) {
        if (event.which !== 13) {
            let searchVal = $("#searchProds").val();
            Session.set("searchVal", searchVal);
        }
    },
    'click #filterStore' (event) {
        event.preventDefault();
        Session.set("searchStore", true);
    },
    'click #closeStoreFilter' (event) {
        event.preventDefault();
        Session.set("searchStore", false);
    },
    'keyup #searchStore' (event) {
        if (event.which !== 13) {
            let searchVal = $("#searchStore").val();
            Session.set("searchStoreVal", searchVal);
        }
    }
});
