import { Products } from '../../../imports/api/products.js';
import { Stores } from '../../../imports/api/stores.js';
import { M } from '../../lib/assets/materialize.js';

Template.prodMgmtForm.onCreated(function() {
    this.subscribe("myProducts");
    this.subscribe("storeInfo");
});

Template.prodMgmtForm.onRendered(function() {
    setTimeout(function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, {});
    }, 200);

    var elemm = document.querySelectorAll('.modal');
    var instancem = M.Modal.init(elemm, {});

    Session.set("prodEditMode", false);
});

Template.prodMgmtForm.helpers({
    stores: function() {
        return Stores.find({}); 
    },
    prodNameErr: function() {
        return Session.get("prodNameRed");
    },
    prodEditMode: function() {
        return Session.get("prodEditMode");
    },
});

Template.prodMgmtForm.events({
    'click .saveProdMgmt, click .editProdMgmt' (event) {
        let name = $("#prodName").val();
        let store = $("#prodStore").val();

        let prodId = Session.get("prodEditId");
        let prodEditMode = Session.get("prodEditMode");

        if (store == null) {
            store = "";
        }

        if (name == "" || name == null) {
            Session.set("prodNameRed", true);
            return;
        } else {
            if (prodEditMode == true) {
                Meteor.call('edit.product', prodId, name, store, function(err, result) {
                    if (err) {
                        console.log("     ERROR: can't add product: " + err);
                    } else {
                        $("#prodName").val("");
                        showSnackbar("Successfully Edited Product!", "green");
                    }
                });
            } else {
                Meteor.call('add.product', name, store, function(err, result) {
                    if (err) {
                        console.log("     ERROR: can't add product: " + err);
                    } else {
                        $("#prodName").val("");
                        showSnackbar("Product Added Succssfully!", "green");
                    }
                });
            }
        }
    },
    'click #showNoStoreSet' (event) {
        let noStoreSet = $("#showNoStoreSet").prop('checked');
        console.log("Clicked: " + noStoreSet);
        if (noStoreSet == true) {
            Session.set("noStoreSet", true);
        } else {
            Session.set("noStoreSet", false);
        }
    }
});
