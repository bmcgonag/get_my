Template.mgmtPage.onCreated(function() {

});

Template.mgmtPage.onRendered(function() {

});

Template.mgmtPage.helpers ({

});

Template.mgmtPage.events ({
    'click .collection-item' (event) {
        event.preventDefault();
        var clickedTarget = event.target.id;
        // console.log("should be going to /" + clickedTarget);
        FlowRouter.go('/' + clickedTarget); 
    },
});