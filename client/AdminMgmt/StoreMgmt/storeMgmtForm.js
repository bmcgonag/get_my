import { Stores } from '../../../imports/api/stores';

Template.storeMgmtForm.onCreated(function() {
    this.subscribe("storeInfo");
});

Template.storeMgmtForm.onRendered(function() {
    Session.set("borderRed", false);
    Session.set("editModeStore", false);
});

Template.storeMgmtForm.helpers({
    editModeStore: function() {
        return Session.get("editModeStore");
    }
});

Template.storeMgmtForm.events({
    'click .saveStoreMgmt' (event) {
        event.preventDefault();
        let storeName = $("#storeName").val();
        if (storeName == "" || storeName == null) {
            Session.set("borderRed", true);
            return;
        } else {
            Meteor.call("add.store", storeName, function(err, result) {
                if (err) {
                    console.log("ERROR: Store add failed: " + err);
                } else {
                    $("#storeName").val("");
                    showSnackbar("Store Added Successfully!", "green");
                }
            });
        }
    },
    'click .cancelStoreMgmt' (event) {
        event.preventDefault();
        $("#storeName").val("");
        Session.set("editModeStore", false);
    },
    // 'submit #storeForm' (event) {
    //     event.preventDefault();
    //     let editMode = Session.get("editModeStore");
    //     let storeName = $("#storeName").val();
    //     if (storeName == "" || storeName == null) {
    //         Session.set("borderRed", true);
    //         return;
    //     } else {
    //         if (editMode == false) {
    //             Meteor.call("add.store", storeName, function(err, result) {
    //                 if (err) {
    //                     console.log("ERROR: Store add failed: " + err);
    //                 } else {
    //                     $("#storeName").val("");
    //                 }
    //             });
    //         } else {
    //             let storeId = Session.get("storeIdEdit");
    //             Meteor.call("edit.store", storeId, storeName, function(err, result) {
    //                 if (err) {
    //                     console.log("ERROR: Store add failed: " + err);
    //                 } else {
    //                     $("#storeName").val("");
    //                     Session.set("editModeStore", false);
    //                 }
    //             });
    //         }
    //     }
    // },
    'click .editStoreMgmt' (event) {
        let storeName = $("#storeName").val();
        let storeId = Session.get("storeIdEdit");
        if (storeName == "" || storeName == null) {
            Session.set("borderRed", true);
            return;
        } else {
            Meteor.call("edit.store", storeId, storeName, function(err, result) {
                if (err) {
                    console.log("ERROR: Store add failed: " + err);
                } else {
                    $("#storeName").val("");
                    Session.set("editModeStore", false);
                    showSnackbar("Store Updated Successfully!", "green");
                }
            });
        }
    }
});