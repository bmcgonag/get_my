import { Stores } from '../../../imports/api/stores';
import { M } from '../../lib/assets/materialize.js';

Template.storeMgmtTbl.onCreated(function() {
    this.subscribe("storeInfo");
});

Template.storeMgmtTbl.onRendered(function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});

    Meteor.setTimeout(function() {
        var elemt = document.querySelectorAll('.tooltipped');
        var instancet = M.Tooltip.init(elemt, {});
    }, 150);
});

Template.storeMgmtTbl.helpers({
    mgmtStoreInfo: function() {
        return Stores.find({});
    },
});

Template.storeMgmtTbl.events({
    'click .deleteStore' (event) {
        event.preventDefault();
        Session.set("deleteId", this._id);
        Session.set("method", "delete.store");
        Session.set("item", this.storeName);
        Session.set("view", "Stores");
    },
    'click .editStore' (event) {
        event.preventDefault();
        Session.set("storeIdEdit", this._id);
        let storeInfo = Stores.findOne({ _id: this._id });
        $("#storeName").val(storeInfo.storeName);
        Session.set("editModeStore", true);
    },
});