import { M } from '../../../lib/assets/materialize.js';
import { Lists } from '../../../../imports/api/lists.js';
import { Menus } from '../../../../imports/api/menu';
import { TaskItems } from '../../../../imports/api/tasks';
import { Products } from '../../../../imports/api/products.js';
import { Stores } from '../../../../imports/api/stores.js';

Template.cleanUpModalConfirm.onCreated(function() {

});

Template.cleanUpModalConfirm.onRendered(function () {

});

Template.cleanUpModalConfirm.helpers({
    whatItems: function() {
        return Session.get("cleanWhat");
    }
});

Template.cleanUpModalConfirm.events({
    'click #confirmClean' (event) {
        let whatItems = Session.get("cleanWhat");

        switch(whatItems) {
            case "Lists":
                cleanUp("clean.Lists", whatItems);
            case "Menus":
                cleanUp("clean.Menus", whatItems);
            case "Products":
                // cleanUp("clean.Products", whatItems);
            case "Stores":
                // cleanUp("clean.Stores", whatItems);
            case "Tasks":
                cleanTasks("clean.Tasks", whatItems);
            default:
                break;
        }
    }
});

cleanUp = function(methodName, whatItems) {
    Meteor.call(methodName, function(err, result) {
        if (err) {
            console.log("    ERROR cleaning " + whatItems + ": " + err);
        } else {
            showSnackbar(whatItems + " have been cleaned up!", "green");
            let confirmModal = document.getElementById('cleanUpConfirm');
            M.Modal.getInstance(confirmModal).close();
        }
    });
}

cleanTasks = function(methodName, whatItems) {
    let timeFrame = Session.get("overdueVal");
    Meteor.call(methodName, timeFrame, function(err, result) {
        if (err) {
            console.log("    ERROR cleaning " + whatItems + ": " + err);
        } else {
            showSnackbar(whatItems + " have been cleaned up!", "green");
            let confirmModal = document.getElementById('cleanUpConfirm');
            M.Modal.getInstance(confirmModal).close();
        }
    });
}