import { M } from '../../lib/assets/materialize.js';
import { Lists } from '../../../imports/api/lists.js';
import { Menus } from '../../../imports/api/menu.js';
import { TaskItems } from '../../../imports/api/tasks.js';
import { Products } from '../../../imports/api/products.js';
import { Stores } from '../../../imports/api/stores.js';

Template.cleanUp.onCreated(function() {
    this.subscribe("allLists");
    this.subscribe("allMenus");
    this.subscribe("allTasks");
    this.subscribe("allProducts");
    this.subscribe("allStores");
});

Template.cleanUp.onRendered(function() {
    var elems = document.querySelectorAll('.tooltipped');
    var instances = M.Tooltip.init(elems, {});

    var elemsm = document.querySelectorAll('.modal');
    var instancesm = M.Modal.init(elemsm, {});

    var elemse = document.querySelectorAll('select');
    var instancese = M.FormSelect.init(elemse, {});

    setTimeout(function() {
        var instances = M.Tooltip.init(elems, {});
        var instancese = M.FormSelect.init(elemse, {});
    }, 300);
});

Template.cleanUp.helpers({

});

Template.cleanUp.events({
    'click #cleanLists' (event) {
        event.preventDefault();
        Session.set("cleanWhat", "Lists");
    },
    'click #cleanMenus' (event) {
        event.preventDefault();
        Session.set("cleanWhat", "Menus");
    },
    'click #cleanProducts' (event) {
        event.preventDefault();
        Session.set("cleanWhat", "Products");
    },
    'click #cleanStores' (event) {
        event.preventDefault();
        Session.set("cleanWhat", "Stores");
    },
    'click #cleanTasks' (event) {
        event.preventDefault();
        Session.set("cleanWhat", "Tasks");
        let timeFrame = $("#removeOverdueTasks").val();
        Session.set("overdueVal", timeFrame);
    },
});