import { SysConfig } from '../../../imports/api/systemConfig.js';
import { M } from '../../lib/assets/materialize.js';

Template.systemAdmin.onCreated(function() {
    this.subscribe("SystemConfig");
    this.subscribe("rolesAvailable");
});

Template.systemAdmin.onRendered(function() {
    this.autorun(() => {
        let curr = SysConfig.findOne({});
        if (typeof curr != 'undefined') {
            $("#allowGenReg").prop('checked', curr.allowReg);
            $("#allAdmReg").prop('checked', curr.SysAdminReg);
            $("#recvUpdateMsgs").prop('checked', curr.allowUpdates);
        } else {
            console.log("    ----    unable to find current system configuration.");
        }
    });

    var elems = document.querySelectorAll('select');

    setTimeout(function() {
        var instances = M.FormSelect.init(elems, {});
    }, 300);

});

Template.systemAdmin.helpers({
    currConfigs: function() {
        
    },
});

Template.systemAdmin.events({
    'change #allowGenReg, change #allowAdmReg' (evnnt) {
        let genReg = $("#allowGenReg").prop('checked');
        let admReg = $("#allowAdmReg").prop('checked');
        // console.log("General Reg set to: " + genReg);
        Meteor.call("add.noSysAdminReg", admReg, genReg, function(err, result) {
            if (err) {
                // console.log("    ERROR updating permission to allow general registration: " + err);
                // showSnackbar("Registration Permission Change Failed.", "red");
            } else {
                // console.log("    Successfully updated permission to allow general registration.");
                showSnackbar("Registration Permission Successfully Changed.", "green")
            }
        });
    },
    'change #recvUpdateMsgs' (event) {
        let updSet = $("#recvUpdateMsgs").prop('checked');
        Meteor.call('allow.updateInfo', updSet, function(err, result) {
            if (err) {
                console.log("    ERROR changing update setting: " + err);
            } else {
                showSnackbar("Update Setting Changed Successfully!", "green");
            }
        });
    },
});
