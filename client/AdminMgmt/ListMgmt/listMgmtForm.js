import { Lists } from '../../../imports/api/lists.js';

Template.listMgmtForm.onCreated(function() {
    this.subscribe("myLists");
});

Template.listMgmtForm.onRendered(function() {
    Session.set("listNameMiss", false);
    Session.set("listNameEditMode", false);
});

Template.listMgmtForm.helpers({
    listNameErr: function() {
        return Session.get("listNameMiss");
    },
    editMode: function() {
        return Session.get("listNameEditMode");
    }
});

Template.listMgmtForm.events({
    'click .saveListMgmt' (event) {
        event.preventDefault();
        let listName = $("#listNameInp").val();
        let shared = $("#isShared").prop('checked');

        if (listName == null || listName == "") {
            Session.set("listNameMiss", true);
            return;
        } else {
            Meteor.call('add.list', listName, shared, function(err, result) {
                if (err) {
                    // console.log("    ERROR adding list name: " + err);
                } else {
                    // console.log("    SUCCESS adding list name.");
                    $("#listNameInp").val("");
                    $("#isShared").prop("checked", false);
                }
            });
        }
    },
    'click .renameListMgmt' (event) {
        event.preventDefault();
        let listName = $("#listNameInp").val();
        let shared = $("#isShared").prop('checked');
        let listId = Session.get("listItemId");

        if (listName == null || listName == "") {
            Session.set("listNameMiss", true);
            return;
        } else {
            Meteor.call('edit.list', listId, listName, shared, function(err, result) {
                if (err) {
                    // console.log("    ERROR editing list name: " + err);
                } else {
                    // console.log("    SUCCESS editing list name.");
                    $("#listNameInp").val("");
                    $("#isShared").prop("checked", false);
                    Session.set("listNameEditMode", false);
                }
            });
        }
    },
    'submit .listAdd' (event) {
        event.preventDefault();
        let editMode = Session.get("listNameEditMode");
        let shared = $("#isShared").prop("checked");
        let listName = $("#listNameInp").val();
        let listId = Session.get("listItemId");

        if (listName == null || listName == "") {
            Session.set("listNameMiss", true);
            return;
        } else {
            if (editMode == false) {
                Meteor.call('add.list', listName, shared, function(err, result) {
                    if (err) {
                        // console.log("    ERROR adding list name: " + err);
                    } else {
                        // console.log("    SUCCESS adding list name.");
                        $("#listNameInp").val("");
                        $("#isShared").prop("checked", false);
                    }
                });
            }   else {
                Meteor.call('edit.list', listId, listName, shared, function(err, result) {
                    if (err) {
                        // console.log("    ERROR editing list name: " + err);
                    } else {
                        // console.log("    SUCCESS editing list name.");
                        $("#listNameInp").val("");
                        $("#isShared").prop("checked", false);
                        Session.set("listNameEditMode", false);
                    }
                });
            }
        }
    }
});