import { Lists } from '../../../imports/api/lists.js';
import { M } from '../../lib/assets/materialize.js';

Template.listMgmtTbl.onCreated(function() {
    this.subscribe("myLists");
});

Template.listMgmtTbl.onRendered(function() {
    var elems = document.querySelectorAll('.tooltipped');
    var instances = M.Tooltip.init(elems, {});
    var elemm = document.querySelectorAll('.modal');
    var instancem = M.Modal.init(elemm, {});
});

Template.listMgmtTbl.helpers({
    lists: function() {
        return Lists.find({});
    }
});

Template.listMgmtTbl.events({
    'click .deleteListName' (event) {
        event.preventDefault();
        Session.set("deleteId", this._id);
        Session.set("method", "delete.list");
        Session.set("item", this.listName);
        Session.set("view", "Lists");
        let listId = this._id;
    },
    'click .editListName' (event) {
        event.preventDefault();
        let listInfo = Lists.findOne({ _id: this._id });
        let listName = listInfo.listName;
        let listShared = listInfo.listShared;
        $("#listNameInp").val(listName);
        if (listShared == true) {
            $("#isShared").prop("checked", true);
        } else {
            $("#isShared").prop("checked", false);
        }
        Session.set("listNameEditMode", true);
        Session.set("listItemId", this._id);
    },
    'click .markListComplete' (event) {
        event.preventDefault();
        let listId = this._id;
        Meteor.call('mark.complete', listId, function(err, result) {
            if (err) {
                // console.log("    ERROR marking complete: " + err);
            } else {
                // console.log("    SUCCESS marking complete.");
            }
        });
    }
});