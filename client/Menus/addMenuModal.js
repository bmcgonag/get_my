import { Menus } from '../../imports/api/menu.js';
import { M } from '../lib/assets/materialize.js';

Template.addMenuModal.onCreated(function() {
    this.subscribe("myMenus");
});

Template.addMenuModal.onRendered(function() {
    Session.set("menuNameErr", false);
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});
});

Template.addMenuModal.helpers({
    menuNameErr: function() {
        return Session.get("menuNameErr");
    },
    editMode: function() {
        return Session.get("menuEditMode");
    }
});

Template.addMenuModal.events({
    'click .saveMenu' (event) {
        event.preventDefault();
        let menuName = $("#menuNameInp").val();
        if (menuName == "" || menuName == null) {
            Session.set("menuNameErr", true);
        } else {
            Meteor.call("add.menu", menuName, function(err, result) {
                if (err) {
                    console.log("    ERROR adding menu: " + err);
                } else {
                    console.log("    SUCCESS adding menu.");
                    $("#menuNameInp").val("");
                }
            });
        }
    },
});