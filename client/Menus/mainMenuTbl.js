import { Menus } from '../../imports/api/menu.js';
import { M } from '../lib/assets/materialize.js';

Template.mainMenuTbl.onCreated(function() {
    this.subscribe("myMenus");
});

Template.mainMenuTbl.onRendered(function() {
    Session.set("menuEditMode", false);
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});
});

Template.mainMenuTbl.helpers({
    myMenus: function() {
        return Menus.find({});
    }
});

Template.mainMenuTbl.events({
    'click li.collection-item' (event) {
        event.preventDefault();
        let sender = event.target;
        // console.log("Sender origination from: ");
        // console.log(sender.localName);
        if (sender.localName == "li") {
            let menuId = event.currentTarget.id;
            if (menuId == "addMenu") {
                // console.log("add menu clicked");
            } else {
                // console.log("menuId is: " + menuId);
                Meteor.call('add.userLast', "Menu", menuId, function(err, result) {
                    if (err) {
                        console.log("    ERROR writing last menu viewed by user to db: " + err);
                    } else {
                        Session.set("menuId", menuId);
                        Meteor.setTimeout(function() {
                            FlowRouter.go('/menuitems');
                        }, 100);  
                    }
                });
            }
        } else if (sender.localName == "i") {
            let menuId = this._id;
            Meteor.call("markMenu.complete", menuId, function(err, result) {
                if (err) {
                    console.log("    ERROR: can't mark menu complete: " + err);
                } else {
                    console.log("    SUCCESS marking menu complete.");
                    Meteor.call('setAllMade.menuItem', menuId, function(err, result) {
                        if (err) {
                            console.log("    ERROR: cannot set all items as made: " + err);
                        } else {
                            console.log("    SUCCESS setting all items made.");
                        }
                    });
                }
            });
        }
    },
});