import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const MScripts = new Mongo.Collection('mScripts');

MScripts.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'set.ScriptRun' (scriptName) {
        check(scriptName, String);

        MScripts.insert({
            scriptName: scriptName,
            hasRun: true,
            runOn: new Date(),
        });
    },
});
