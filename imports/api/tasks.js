import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import moment from 'moment';

export const TaskItems = new Mongo.Collection('taskitems');

TaskItems.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.task' (taskNameArr, assignedTo, assignedToId, taskDateArr, actDate) {
        check(taskNameArr, [Object]);
        check(assignedTo, String);
        check(taskDateArr, [String]);
        check(assignedToId, String);
        check(actDate, [Date]);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add tasks. Make sure you are logged in with valid user credentials.');
        }

        let username;

        if (assignedTo == "self") {
            let userInfo = Meteor.users.findOne({ _id: this.userId });
            username = userInfo.profile.fullname;
            assignedToId = this.userId;
        } else {
            username = assignedTo;
        }

        for (i=0; i < taskDateArr.length; i++) {
            for (j=0; j < taskNameArr.length; j++) {
                TaskItems.insert({
                    taskName: taskNameArr[j].id,
                    taskDate: taskDateArr[i],
                    actualDate: actDate[i],
                    assignedTo: username,
                    assignedToId: assignedToId,
                    isComplete: false,
                    completedOn: null,
                    assignedOn: new Date(),
                    assignedBy: this.userId,
                });
            }
        }
    },
    'add.mytask' (taskName, assignedTo, assignedToId, taskDate, actDate) {
        check(taskName, String);
        check(taskDate, String);
        check(actDate, Date);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add tasks. Make sure you are logged in with valid user credentials.');
        }

        let username;

        if (assignedTo == "self") {
            let userInfo = Meteor.users.findOne({ _id: this.userId });
            username = userInfo.profile.fullname;
            assignedToId = this.userId;
        } else {
            username = assignedTo;
        }

        return TaskItems.insert({
            taskName: taskName,
            taskDate: taskDate,
            actualDate: actDate,
            assignedTo: username,
            assignedToId: assignedToId,
            isComplete: false,
            completedOn: null,
            assignedOn: new Date(),
            assignedBy: this.userId,
        });
    },
    'edit.task' (taskId, taskName, assignedTo, taskDate) {
        check(taskId, String);
        check(taskName, String);
        check(assignedTo, String);
        check(taskDate, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit tasks. Make sure you are logged in with valid user credentials.');
        }

        return TaskItems.update({ _id: taskId }, {
            $set: {
                taskName: taskName,
                taskDate: taskDate,
                assignedTo: assignedTo,
                changedOn: new Date(),
                changedBy: this.userId,
            }
        });
    },
    'delete.task' (taskId) {
        check(taskId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete tasks. Make sure you are logged in with valid user credentials.');
        }

        return TaskItems.remove({ _id: taskId });
    },
    'markTask.complete' (taskId) {
        check(taskId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to mark tasks complete. Make sure you are logged in with valid user credentials.');
        }

        return TaskItems.update({ _id: taskId }, {
            $set: {
                isComplete: true,
                completedOn: new Date(),
                completedBy: this.userId,
            }
        });
    },
    'markTask.notComplete' (taskId) {
        check(taskId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to mark tasks not complete. Make sure you are logged in with valid user credentials.');
        }

        return TaskItems.update({ _id: taskId }, {
            $set: {
                isComplete: false,
                markedUncomplteOn: new Date(),
                markedUncompleteBy: this.userId,
            }
        });
    },
    'clean.Tasks' (timeFrame) {
        check(timeFrame, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to clean up old Tasks. Make sure you are logged in with valid user credentials.');
        }

        let d = new Date();
        let upToDate = "";
        switch(timeFrame) {
            case "1-week":
                console.log("1 Week");
                upToDate = 7 * 24 * 60 * 60 * 1000;
                break;
            case "2-weeks":
                console.log("2 Week");
                upToDate = 14 * 24 * 60 * 60 * 1000;
                break;
            case '1-month':
                console.log("1 month");
                upToDate = 30 * 24 * 60 * 60 * 1000;
                break;
            case '3-months':
                console.log("3 months");
                upToDate = 90 * 24 * 60 * 60 * 1000;
                break;
            case 'all':
                console.log("all");
                upToDate = 1 * 24 * 60 * 60 * 1000;
                break;
            default:
                break;
        }

        return TaskItems.remove({ actualDate: { $lt: new Date((new Date()) - upToDate )}});
    }
});