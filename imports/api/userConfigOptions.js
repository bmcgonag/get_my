import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const UserConfigOptions = new Mongo.Collection('userConfigOptions');

UserConfigOptions.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'change.userPass' (usersId, password) {
        check(usersId, String);
        check(password, String);

        return Accounts.setPassword(usersId, password);
    },
});