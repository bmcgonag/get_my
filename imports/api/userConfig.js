import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const UserConfig = new Mongo.Collection('userConfig');

UserConfig.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.darkModePref' (pref) {
        check(pref, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change registration setting. Make sure you are logged in with valid system administrator credentials.');
        }

        return UserConfig.insert({
            user: this.userId,
            darkMode: pref,
            dateAdded: Date()
        });
    },
    'update.darkModePref' (pref) {
        check(pref, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change registration setting. Make sure you are logged in with valid system administrator credentials.');
        }

        let myConfig = UserConfig.findOne({ user: this.userId });
        if (typeof myConfig == 'undefined') {
            Meteor.call('add.darkModePref', pref, function(err, result) {
                if (err) {
                    console.log("    ERROR calling the add functioin for dark mode: " + err);
                } 
            });
        } else {
            return UserConfig.update({ user: this.userId }, {
                $set: {
                    darkMode: pref,
                    dateUpdate: Date()
                }
            });
        }
    }
});