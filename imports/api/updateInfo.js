import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const UpdateInfo = new Mongo.Collection('updateInfo');

UpdateInfo.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.updateInfo' (updateObject) {
        check(updateObject, Object);

        UpdateInfo.insert({
            title: updateObject.title,
            description: updateObject.description,
            dateRelease: updateObject.date,
            releaseLink: updateObject.link
        });
    },
    'markUpdate.read' (updateId) {
        check(updateId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to mark updates as read. Make sure you are logged in with valid user credentials.');
        }

        return UpdateInfo.update({ _id: updateId }, {
            $set: {
                viewed: true
            }
        });
    }
});
