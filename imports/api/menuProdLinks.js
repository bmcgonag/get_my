import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Products } from './products';

export const MenuProdLinks = new Mongo.Collection('menuProdLinks');

MenuProdLinks.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.menuProdLinks' (menuItemId, menuItemName, prodNameArray) {
        check(menuItemId, String);
        check(menuItemName, String);
        check(prodNameArray, [Object]);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add menu and product links. Make sure you are logged in with valid user credentials.');
        };

        let linkExists = MenuProdLinks.findOne({ menuItemId: menuItemId });

        if (linkExists) {
            console.log("sending to update method instead.");
            Meteor.call('update.menuPordLInks', menuItemId, menuItemName, prodNameArray, function(err, result) {
                if (err) {
                    console.log("    ERROR moving to the update method: " + err);
                } else {
                    console.log("Successfully updated the menu prod links.")
                }
            });
        } else {
            return MenuProdLinks.insert({
                menuItemId: menuItemId,
                menuItemName: menuItemName,
                products: prodNameArray,
                dateCreated: Date(),
                createdBy: this.userId
            });
        }
    },
    'update.menuPordLInks' (menuItemId, menuItemName, prodNameArray) {
        check(menuItemId, String);
        check(menuItemName, String);
        check(prodNameArray, [Object]);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add menu and product links. Make sure you are logged in with valid user credentials.');
        };

        return MenuProdLinks.update({ menuItemId: menuItemId }, {
            $set: {
                products: prodNameArray,
                dateUpdated: Date(),
                updatedBy: this.userId
            }
        });
    },
});
