import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const SysConfig = new Mongo.Collection('sysConfig');

SysConfig.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.noSysAdminReg' (admReg, genReg) {
        check(admReg, Boolean);
        check(genReg, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change registration setting. Make sure you are logged in with valid system administrator credentials.');
        }

        let curr = SysConfig.findOne({});
        if (typeof curr != 'undefined') {
            let configId = curr._id;
            Meteor.call('edit.noSysAdminReg', configId, admReg, genReg, function(err, result) {
                if (err) {
                    console.log("    ERROR updating sys admin reg: " + err);
                } else {
                    console.log("Success updating sys admin reg.");
                }
            });
        } else {
            return SysConfig.insert({
                SysAdminReg: admReg,
                dateAdded: new Date(),
                allowReg: genReg,
            });
        }
    },
    'edit.noSysAdminReg' (configId, canReg, genReg) {
        check(canReg, Boolean);
        check(configId, String);
        check(genReg, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change registration setting. Make sure you are logged in with valid system administrator credentials.');
        }

        return SysConfig.update({ _id: configId }, {
            $set: {
                SysAdminReg: canReg,
                allowReg: genReg,
                dateUpdated: new Date(),
            }
        });
    },
    'allow.updateInfo' (allowUpdate) {
        check(allowUpdate, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change system update notification settings. Make sure you are logged in with valid system administrator credentials.');
        }

        let curr = SysConfig.findOne({});

        let configId = curr._id;

        return SysConfig.update({ _id: configId }, {
            $set: {
                allowUpdates: allowUpdate,
            }
        });
    },
});
