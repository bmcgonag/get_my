import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Lists = new Mongo.Collection('lists');

Lists.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.list' (listName, isShared) {
        check(listName, String);
        check(isShared, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add lists. Make sure you are logged in with valid user credentials.');
        }

        return Lists.insert({
            listName: listName,
            listShared: isShared,
            listOwner: this.userId,
            listComplete: false,
        });
    },
    'edit.list' (listId, listName, isShared) {
        check(listId, String);
        check(listName, String);
        check(isShared, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit lists. Make sure you are logged in with valid user credentials.');
        }

        return Lists.update({ _id: listId }, {
            $set: {
                listName: listName,
                listShared: isShared,
            }
        });
    },
    'delete.list' (listId) {
        check(listId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete lists. Make sure you are logged in with valid user credentials.');
        }

        return Lists.remove({ _id: listId });
    },
    'mark.complete' (listId) {
        check(listId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to mark lists complete. Make sure you are logged in with valid user credentials.');
        }

        return Lists.update({ _id: listId }, {
            $set: {
                listComplete: true,
                completedOn: new Date()
            }
        });;
    },
    'clean.Lists' () {
        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to clean up old lists. Make sure you are logged in with valid user credentials.');
        }

        return Lists.remove({ listComplete: true });
    },
});