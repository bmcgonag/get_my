import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { MenuItems } from './menuItems';
import moment from 'moment';
import { mark } from '@babel/runtime/regenerator';

export const Menus = new Mongo.Collection('menus');

Menus.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.menu' (menuName) {
        check(menuName, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add menus. Make sure you are logged in with valid user credentials.');
        }

        return Menus.insert({
            menuName: menuName,
            menuOwner: this.userId,
            menuComplete: false,
        });
    },
    'edit.menu' (menuId, menuName) {
        check(menuId, String);
        check(menuName, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit menus. Make sure you are logged in with valid user credentials.');
        }

        return Menus.update({ _id: menuId }, {
            $set: {
                menuName: menuName,
            }
        });
    },
    'delete.menu' (menuId) {
        check(menuId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete menus. Make sure you are logged in with valid user credentials.');
        }

        return Menus.remove({ _id: menuId });
    },
    'markMenu.complete' (menuId) {
        check(menuId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to mark menus complete. Make sure you are logged in with valid user credentials.');
        }

        return Menus.update({ _id: menuId }, {
            $set: {
                menuComplete: true,
            }
        });
    },
    'markMenu.notComplete' (menuId) {
        check(menuId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to mark menus not complete. Make sure you are logged in with valid user credentials.');
        }

        return Menus.update({ _id: menuId }, {
            $set: {
                menuComplete: false,
            }
        });
    },
    'clean.Menus' () {
        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to clean up old Menus. Make sure you are logged in with valid user credentials.');
        }

        let removeMenuIds = [];
        

        // let's find all the menus with menu items that are past their date.
        let menuList = Menus.find({ menuComplete: false }).fetch();
        for (i=0; i < menuList.length; i++) {
            let removeMenu = true;
            let items = MenuItems.find({ menuId: menuList[i]._id }).fetch();
            for (j=0; j < items.length; j++) {
                let srvDate = moment(items[j].serveDateActual);
                let today = moment();
                let expired = moment(today).isAfter(srvDate);
                if (expired != true) {
                    removeMenu = false;
                }
                if (j == items.length - 1) {
                    if (removeMenu == true) {
                        removeMenuIds.push(menuList[i]._id);
                    }
                }
            }
        }

        // next let's add the ids of any menus that are marked complete
        let markedComplete = Menus.find({ menuComplete: true }).fetch();
        for (k = 0; k < markedComplete.length; k++) {
            let menuId = markedComplete[k]._id;
            removeMenuIds.push(menuId);
        }

        // finally we'll cycle through the ids and remove the items we collected up.
        for (l = 0; l < removeMenuIds.length; l++) {
            Menus.remove({ _id: removeMenuIds[l] });
        }
    },
    'addto.Menu' (menuId, menuItem, menuItemId, dateSrv, isLinked) {
        check(menuId, String);
        check(menuItem, String);
        check(menuItemId, String);
        check(dateSrv, String);
        check(isLinked, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add items to menus. Make sure you are logged in with valid user credentials.');
        }

        serveDateActual = new Date(dateSrv);

        return Menus.update({ _id: menuId }, {
            $addToSet: {
                menuItems:
                {
                    menuItemId: menuItemId,
                    menuItemName: menuItem,
                    serveDate: dateSrv,
                    serveDateActual: serveDateActual,
                    isLinked: isLinked
                },
            }
        });
    },
    'link.inMenu' (menuItemId, isLinked) {
        check(menuItemId, String);
        check(isLinked, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to link menu items to products. Make sure you are logged in with valid user credentials.');
        }

        return Menus.update({ 'menuItems.menuItemId': menuItemId }, {
            $set: {
                "menuItems.$.isLinked": isLinked
            }
        });
    },
    'delete.itemFromMenu' (itemIds) {
        check(itemIds, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete items from a menu. Make sure you are logged in with valid user credentials.');
        }

        let ids = itemIds.split('_');
        console.log("item ids: " + ids[0] + " and " + ids[1]);

        return Menus.update({ _id: ids[0] }, {
            $pull: {
                menuItems: {
                    menuItemId: ids[1],
                },
            }
        });
    }
});
