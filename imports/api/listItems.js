import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Products } from './products';
import { ShopLists } from './shopList';

export const ListItems = new Mongo.Collection('listitems');

ListItems.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.listItem' (itemName, listId) {
        check(itemName, String);
        check(listId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add items. Make sure you are logged in with valid user credentials.');
        }

        let iname = itemName.charAt(0).toUpperCase() + itemName.slice(1);

        // look up the item from the Products collection
        let prodInfo = Products.findOne({ prodName: iname });
        if (!prodInfo) {
            Meteor.call("add.product", itemName, [""], function(err, result) {
                if (err) {
                    console.log("   ERROR adding item to products: " + err);
                } else {
                    // console.log("    SUCCESS adding item to Products.");

                    return ListItems.insert({
                        itemName: iname,
                        listId: listId,
                        prodId: result,
                        addedBy: this.userId,
                        itemStore: "",
                        itemOrdered: false,
                        itemReceived: false,
                        dateAddedToList: new Date(),
                    });
                }
            });
        } else {
            return ListItems.insert({
                itemName: iname,
                listId: listId,
                prodId: prodInfo._id,
                addedBy: this.userId,
                itemStore: prodInfo.prodStore,
                itemOrdered: false,
                itemReceived: false,
                dateAddedToList: new Date(),
            });
        }
    },
    'add.itemsFromMenuItem' (itemIds, listId) {
        check(itemIds, [String]);
        check(listId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add items from a menu. Make sure you are logged in with valid user credentials.');
        }

        console.dir(itemIds);

        for (i=0; i < itemIds.length; i++) {
            // let's check and make sure the product isn't already on the list
            let onList = ListItems.find({ listId: listId, prodId: itemIds[i] }).count();
            console.log("Number On List: " + onList);
            if (onList == 0) {
                // now pull the product
                let prodInfo = Products.findOne({ _id: itemIds[i] });
                ListItems.insert({
                    itemName: prodInfo.prodName,
                    listId: listId,
                    prodId: prodInfo._id,
                    addedBy: this.userId,
                    itemStore: prodInfo.prodStore,
                    itemOrdered: false,
                    itemReceived: false,
                    dateAddedToList: new Date(),
                });
            } else {
                // product exists on list, move on to next
                console.log("Product Exists on Selected List.");
            }
        }
    },
    'setOrdered.listItem' (itemId) {
        check(itemId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to set items as ordered. Make sure you are logged in with valid user credentials.');
        }

        return ListItems.update({ _id: itemId }, {
            $set: {
                itemOrdered: true,
                dateOrdered: new Date(),
            }
        });
    },
    'setAllOrdered.listItem' (shopListId) {
        // set all items that are not already set as ordered, or set as received to ordered on this list

    },
    'setNotOrdered.listItem' (itemId) {
        check(itemId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to set items as not ordered. Make sure you are logged in with valid user credentials.');
        }

        return ListItems.update({ _id: itemId }, {
            $set: {
                itemOrdered: false,
                dateUnOrdered: new Date(),
            }
        });
    },
    'setReceived.listItem' (itemId) {
        check(itemId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to set items as received. Make sure you are logged in with valid user credentials.');
        }

        return ListItems.update({ _id: itemId }, {
            $set: {
                itemReceived: true,
                dateReceived: new Date(),
            }
        });

    },
    'setNotReceived.listItem' (shopListId) {
        check(itemId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to set items as not received. Make sure you are logged in with valid user credentials.');
        }

        return ListItems.update({ _id: itemId }, {
            $set: {
                itemReceived: false,
                dateNotReceived: new Date(),
            }
        });
    },
    'setAllReceived.listItem' () {
        // set all items that are not already listed as received to received on this list

    },
    'edit.listItem' (itemId, itemName) {
        check(itemId, String);
        check(itemName, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add items. Make sure you are logged in with valid user credentials.');
        }

        return ListItems.update({ _id: itemId }, {
            $set: {
                itemName: itemName,
            }
        });
    },
    'delete.listItem' (itemId) {
        check(itemId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete list items. Make sure you are logged in with valid user credentials.');
        }

        return ListItems.remove({ _id: itemId });
    }
});
