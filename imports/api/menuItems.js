import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const MenuItems = new Mongo.Collection('menuitems');

MenuItems.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.menuItem' (itemName) {
        check(itemName, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add items. Make sure you are logged in with valid user credentials.');
        }

        return MenuItems.insert({
            itemName: itemName,
            addedBy: this.userId,
            dateAddedtoMenu: new Date(),
            isLinked: false,
        });
    },
    'update.menuItemLinked' (itemId, isLinked) {
        check(itemId, String);
        check(isLinked, Boolean);
        
        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to set this menu item as linked to products. Make sure you are logged in with valid user credentials.');
        }

        return MenuItems.update({ _id: itemId }, {
            $set: {
                isLinked: isLinked,
            }
        });
    },
    'edit.madeItem' (itemId, itemName) {
        check(itemId, String);
        check(itemName, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit menu items. Make sure you are logged in with valid user credentials.');
        }

        return MenuItems.update({ _id: itemId }, {
            $set: {
                itemName: itemName,
            }
        });
    },
    'delete.menuItem' (itemId) {
        check(itemId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete menu items. Make sure you are logged in with valid user credentials.');
        }

        return MenuItems.remove({ _id: itemId });
    },
    'shiftDate' (itemId, momentAddDay) {
        check(itemId, String);
        check(momentAddDay, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to shift menu item dates. Make sure you are logged in with valid user credentials.');
        }

        return MenuItems.update({ _id: itemId }, {
            $set: {
                serveDate: momentAddDay,
            }
        });
    }
});
