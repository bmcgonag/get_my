import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const UserLast = new Mongo.Collection('userLast');

UserLast.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.userLast' (view, viewId) {
        check(view, String);
        check(viewId, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change user view last setting. Make sure you are logged in with valid system administrator credentials.');
        }

        // first let's find out if there's an entry for this user and view, and if so
        // we'll just edit that entry with updated information
        let userListInfo = UserLast.findOne({ userId: this.userId, view: view });
        if (typeof userListInfo != 'undefined' && userListInfo != "" && userListInfo != null) {
            // entry exists, call the edit function instead
            Meteor.call('edit.userLast', view, viewId, function(err, result) {
                if (err) {
                    console.log("    ERROR moving user to edit for last view: " + err);
                }
            });
        } else {
            return UserLast.insert({
                userId: this.userId,
                view: view,
                viewId: viewId,
                dateAdded: Date(),
            });
        }
    },
    'edit.userLast' (view, viewId) {
        check(view, String);
        check(viewId, String);

        if (!this.userId) {
            throw new Meteor.Error('Not able to change user view last setting. Make sure you are logged in with valid system administrator credentials.');
        }

        return UserLast.update({ view: view, userId: this.userId }, {
            $set: {
                viewId: viewId,
                dateLastUpdate: Date(),
            }
        });
    }
});