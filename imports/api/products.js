import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Products = new Mongo.Collection('products');

Products.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.product' (prodName, prodStore) {
        check(prodName, String);
        check(prodStore, [String]);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to add products. Make sure you are logged in with valid user credentials.');
        }

        let pname = prodName.charAt(0).toUpperCase() + prodName.slice(1);

        // first does this product already exist?

        let prodExists = Products.findOne({ prodName: pname });

        if (!prodExists) {
            return Products.insert({
                prodName: pname,
                prodOwner: this.userId,
                prodStore: prodStore,
            });
        }
    },
    'edit.product' (prodId, prodName, prodStore) {
        check(prodId, String);
        check(prodName, String);
        check(prodStore, [String]);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to edit products. Make sure you are logged in with valid user credentials.');
        }

        let pname = prodName.charAt(0).toUpperCase() + prodName.slice(1);

        return Products.update({ _id: prodId }, {
            $set: {
                prodName: pname,
                prodStore: prodStore,
            }
        });
    },
    'delete.product' (prodId) {
        check(prodId, String);

        if (!this.userId) {
            throw new Meteor.Error('You are not allowed to delete products. Make sure you are logged in with valid user credentials.');
        }

        return Products.remove({ _id: prodId });
    }
});
