import { SysConfig } from '../imports/api/systemConfig.js';
import { Stores } from '../imports/api/stores.js';
import { UserConfigOptions } from '../imports/api/userConfigOptions.js';
import { Products } from '../imports/api/products.js';
import { Lists } from '../imports/api/lists.js';
import { ListItems } from '../imports/api/listItems.js';
import { Menus } from '../imports/api/menu.js';
import { MenuItems } from '../imports/api/menuItems.js';
import moment from 'moment';
import { TaskItems } from '../imports/api/tasks.js';
import { UserConfig } from '../imports/api/userConfig.js';
import { MenuProdLinks } from '../imports/api/menuProdLinks.js';
import { UserLast } from '../imports/api/userLast.js';
import { UpdateInfo } from '../imports/api/updateInfo.js';

Meteor.publish("SystemConfig", function() {
    try {
        return SysConfig.find({});
    } catch (error) {
        console.log("    ERROR pulling system config data: " + error);
    }
});

Meteor.publish("UserConfigPrefs", function() {
    try {
        return UserConfig.find({});
    } catch (error) {
        console.log("    ERROR: Error accessing user config: " + error);
    }
});

Meteor.publish("UpdateVersion", function() {
    try {
        return UpdateInfo.find({ viewed: false });
    } catch(error) {
        console.log("    ERROR pulling updated version info: " + error);
    }
})

Meteor.publish('userList', function() { 
    return Meteor.users.find({});
});

Meteor.publish("userLastView", function() {
    try {
        return UserLast.find({ userId: this.userId });
    } catch (error) {
        console.log("    ----    ERROR pulling user last info: " + error);
    }
});

Meteor.publish("storeInfo", function() {
    try {
        return Stores.find({});
    } catch (error) {
        console.log("    ERROR pulling store data: " + error);
    }
});

Meteor.publish("myProducts", function() {
    try {
        return Products.find({});
    } catch (error) {
        console.log("    ERROR pulling product data: " + error);
    }
});

Meteor.publish("limProducts", function(findItemVal) {
    try {
        return Products.find({ prodName: {$regex: findItemVal + '.*', $options: 'i' }});
    } catch (error) {
        cconsole.log("    ERROR pulling limited product data: " + error);
    }
})

Meteor.publish("myLists", function() {
    try {
        return Lists.find( { $or: [ { listOwner: this.userId, listComplete: false }, { listShared: true, listComplete: false } ] } );
    } catch (error) {
        console.log("    ERROR pulling list data: " + error);
    }
});

Meteor.publish("myListItems", function(listId) {
    try {
        return ListItems.find({ listId: listId });
    } catch (error) {
        console.log("    ERROR pulling list items for this list: " + error);
    }
});

Meteor.publish("myStoreListItems", function(listId) {
    try {
        let stores = Store.find({});
        if (stores) {
            for (i=0; i<stores.length; i++) {
                let items = ListItems.find({ prodStore: store[i], listId: listId }).fetch();

            }
        }
    } catch (error) {
        console.log("    ERROR pulling items or stores: " + error);
    }
});

Meteor.publish("myMenus", function() {
    try {
        return Menus.find({ menuComplete: false });
    } catch (error) {
        console.log("    ERROR pulling menu info: " + error);
    }
});

Meteor.publish("menuItems", function() {
    try {
       return MenuItems.find({});
    } catch (error) {
        console.log("    ERROR pulling menuItem data: " + error);
    }
});

Meteor.publish("myMenuItems", function(menuId) {
    try {
        return Menus.find({ menuId: menuId });
    } catch (error) {
        console.log("    ERROR pulling menu items for this list: " + error);
    }
});

Meteor.publish("allMenuItems", function() {
    try {
        return MenuItems.find({});
    } catch (error) {
        console.log("    ERROR pulling all menu items from collection: " + error);
    }
})

Meteor.publish("todayMenuItems", function() {
    try {
        let todayDate = new Date();
        let todaysDate = moment(todayDate).format("MMM DD, YYYY");
        return Menus.find({ 'menuItems.serveDate': todaysDate });
    } catch (error) {
        console.log("    ERROR pulling today's menu items: " + error);
    }
});

Meteor.publish("allTasks", function() {
    try {
        return TaskItems.find({});
    } catch (error) {
        console.log("    ERROR pulling the task items: " + error);
    }
});

Meteor.publish("allProducts", function() {
    try {
        return Products.find({});
    } catch (error) {
        console.log("    ERROR pulling the task items: " + error);
    }
});

Meteor.publish("allMenus", function() {
    try {
        return Menus.find({});
    } catch (error) {
        console.log("    ERROR pulling the task items: " + error);
    }
});

Meteor.publish("allStores", function() {
    try {
        return Stores.find({});
    } catch (error) {
        console.log("    ERROR pulling the task items: " + error);
    }
});

Meteor.publish("allLists", function() {
    try {
        return Lists.find({});
    } catch (error) {
        console.log("    ERROR pulling the task items: " + error);
    }
});

Meteor.publish("myTasks", function() {
    try {
        return TaskItems.find({ assignedToId: this.userId });
    } catch (error) {
        console.log("    ERROR pulling the task items: " + error);
    }
});

Meteor.publish("rolesAvailable", function() {
    try {
        return Meteor.roles.find({});
    } catch (error) {
        console.log("    ERROR publishing roles: " + error);
    }
});

Meteor.publish("menuProdLinkData", function() {
    try {
        return MenuProdLinks.find({});
    } catch (error) {
        console.log("    ERROR publishing menu product links: " + error);
    }
});
