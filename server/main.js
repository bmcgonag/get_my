import { Meteor } from 'meteor/meteor';
import { SysConfig } from '../imports/api/systemConfig';
import { MenuItems } from '../imports/api/menuItems';
import { Products } from '../imports/api/products.js';
import { Menus } from '../imports/api/menu.js';
import { MScripts } from '../imports/api/mScripts.js';
import { UpdateInfo } from '../imports/api/updateInfo.js';

Meteor.startup(() => {
  // code to run on server at startup
  Roles.createRole("user", {unlessExists: true});
  Roles.createRole("admin", {unlessExists: true});
  Roles.createRole("systemadmin", {unlessExists: true});

  // set the systemconfig defaults for registration
  // check if this has already been run
  let regPolRun = MScripts.findOne({ scriptName: "DefaultRegPolicy", scriptRun: true });
  if (typeof regPolRun == "undefined" || regPolRun == null || regPolRun == "") {
    let regPolicy = SysConfig.findOne({});
    if (typeof regPolicy == 'undefined') {
      return SysConfig.insert({
        SysAdminReg: false,
        dateAdded: new Date(),
        allowReg: true,
        allowUpdates: true,
      });
    } else {
      // console.log("Registration policy already set.");
      markScriptRun("DefaultRegPolicy");
    }
  }


  // check if the isLInked item exists on menuitems, and if not, add it (data cleanup task)
  // see if already updated
  let linkUpdateRun = MScripts.findOne({ scriptName: "updateMenuItemLinks", scriptRun: true });
  if (typeof linkUpdateRun == 'undefined' || linkUpdateRun == null || linkUpdateRun == "") {
    let itemInfoNoLink = MenuItems.find({ isLinked: { $exists: false } }).fetch();
    // console.log("No Ites with isLinked not set: " + itemInfoNoLink.length);
    if (itemInfoNoLink.length > 0) {
      // console.log("found items with isLinked not set.");
      // console.dir(itemInfoNoLink);
      let infoLength = itemInfoNoLink.length;
      for (i=0; i < infoLength; i++) {
        MenuItems.update({ _id: itemInfoNoLink[i]._id }, {
          $set: {
              isLinked: false,
          }
        });
        if (i == (infoLength -1)) {
          markScriptRun("updateMenuItemLinks");
        }
      }
    } else {
      // this will show if all items are found to have isLInked set.
      // console.log("No items with isLinked not set.");
      markScriptRun("updateMenuItemLinks");
    }
  }


  // update Products to be able to have multiple stores in the document
  // check if update already run
  let prodStoreArrayRun = MScripts.findOne({ scriptName: "changeProdStoreToArray", scriptRun: true });
  if (!prodStoreArrayRun) {
    let prodInfo = Products.find({}).fetch();
    let prodCount = prodInfo.length;
    console.log("Updating Products to allow multiple store assocation for " + prodCount + " products.");
    for (j = 0; j < prodCount; j++) {
      if (typeof prodInfo[j].prodStore == 'object') {
        // console.log(typeof prodInfo[j].prodStore);
        // console.log("Is Array already");
      } else {
        let prodStoreArray = [];
        // console.log("----    ----    ----");
        // console.log(typeof prodInfo[j].prodStore);
        // console.log("---- Is Not Array.");
        let prodStore = prodInfo[j].prodStore;

        prodStoreArray.push(prodStore);
        // console.dir(prodStoreArray);
        Products.update({ _id: prodInfo[j]._id }, {
          $set: {
            prodStore: prodStoreArray,
          }
        });
      }
      if (j == (prodCount -1)) {
        markScriptRun("changeProdStoreToArray");
      }
    }
  }

  // update menu items to new format so they get the linked products
  // check if this update has run
  let menuItemUpdRun = MScripts.findOne({ scriptName: "updateMenuProdLinks", scriptRun: true });
  if (!menuItemUpdRun) {
    let openMenus = Menus.find({ menuComplete: false }).fetch();
    let openMenuCount = openMenus.length;
    // console.log("Open Menu count is: " + openMenuCount);
    for (k = 0; k < openMenuCount; k++) {
      if (typeof openMenus.menuItems == 'object') {
        // console.log(openMenus.menuName + " appears to be converted.");
        markScriptRun("updateMenuProdLinks");
      } else {
        let menuId = openMenus[k]._id;

        let thisMenuItems = MenuItems.find({ menuId: menuId }).fetch();

        let itemCount = thisMenuItems.length;

        for (l = 0; l < itemCount; l++) {
          Menus.update({ _id: menuId }, {
            $addToSet: {
              menuItems:
              {
                menuItemId: thisMenuItems[l]._id,
                menuItemName: thisMenuItems[l].itemName,
                serveDate: thisMenuItems[l].serveDate,
                serveDateActual: thisMenuItems[l].serveDateActual,
                isLinked: thisMenuItems[l].isLinked
              },
            }
          });
        }
      }
      if (k == (openMenuCount - 1)) {
        markScriptRun("updateMenuProdLinks");
      }
    }
  }

  // get update available information if enabled in system confiuration
  let currConfig = SysConfig.findOne({});
  let feedurl = "https://gitlab.com/bmcgonag/get_my/-/releases.atom"
  if (currConfig.allowUpdates == true) {
    // console.log("Allow Updates is true");
    startCronForUpdates(feedurl);
  } else if (typeof currConfig.allowUpdates == 'undefined' || currConfig.allowUpdates == null) {
    SysConfig.update({ _id: currConfig._id }, { $set: {
      allowUpdates: true,
    }});
    startCronForUpdates(feedurl);
  }
});

var startCronForUpdates = function(feedurl) {
  var cron = require('node-cron');

  cron.schedule('*/30 * * * *', () => {
    getUpdateInfoNow(feedurl);
  });
}

var markScriptRun = function(scriptName) {
  // check if this is already set
  let scriptRun = MScripts.findOne({ scriptName: scriptName });

  if (scriptRun) {
    console.log(scriptName + " already set as run on " + scriptRun.runOn);
  } else {
    MScripts.insert({
        scriptName: scriptName,
        scriptRun: true,
        runOn: new Date()
      });
  }
}

var getUpdateInfoNow = async function(feedurl) {
    const parser = require('rss-url-parser')

    const data = await parser(feedurl)
    let dataLength = data.length;
    // console.dir(data[0].title);

    // check if this title already exists in db
    let updatesExist = UpdateInfo.findOne({ title: data[0].title });
    if (!updatesExist) {
      UpdateInfo.insert({
          title: data[0].title,
          description: data[0].description,
          dateRelease: data[0].date,
          releaseLink: data[0].link,
          viewed: false
      });
    } else {
      console.log("No new updates available at this time.");
    }
}
