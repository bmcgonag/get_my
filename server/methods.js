import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { TaskItems } from '../imports/api/tasks';
import { SysConfig } from '../imports/api/systemConfig';

Meteor.methods({
    'addToRole' (role) {
        let countOfUsers = Meteor.users.find().count();

        // if users = 1 - set to role passed in function call
        if (countOfUsers > 1) {
            console.log("User id for role: " + Meteor.userId() );
            let userId = Meteor.userId();
            Roles.addUsersToRoles(userId, role);
            Meteor.call('add.darkModePref', "light", function(err, result) {
                if (err) {
                    console.log("    ERROR: can't set user dark mode preference: " + err);
                } else {
                    // console.log("    SUCCESSFULLY set user dark mode preference.");
                }
            });
        } else if (countOfUsers == 1) {
            console.log("Creating first system admin user: " + Meteor.userId() );
            let userId = Meteor.userId();
            Roles.addUsersToRoles(userId, "systemadmin");
            Meteor.call('add.darkModePref', "light", function(err, result) {
                if (err) {
                    console.log("    ERROR: can't set user dark mode preference: " + err);
                } else {
                    // console.log("    SUCCESSFULLY set user dark mode preference.");
                }
            });
        }
    },
    'edit.userPass' (userId, newPassword) {
        check(userId, String);
        check(newPassword, String);

        return Accounts.setPassword(userId, newPassword);
    },
    'delete.userFromSys' (userId) {
        check(userId, String);

        return Meteor.users.remove({ _id: userId });
    },
    'update.userEmail' (userId, email) {
        check(userId, String);
        check(email, String);

        return Meteor.users.update({ _id: userId }, {
            $set: {
                'emails.0.address': email,
            }
        });
    },
    'edit.userRole' (userId, role) {
        check(userId, String);
        check(role, String);

        return Roles.setUserRoles(userId, role);
    },
});
