# Get My
Open source shopping / list app.

## NOTE: 
If you were using this previously, the new updates change the data structure. Please remove your old database if you find odd behavior or errors. The data structures may simply be incompatible. 
I apologize for any inconvenince as I simplify the app a bit.

## Install and Run
NOTE: if you have a cpu without the AVX instruction set, you can change the line 'image: mongo' to be 'image: mongo:4.4'in the docker compose below.

### Docker / Docker Compose
You can use this docker compose file to get the system up and running.

```
---
services:
  get_my:
    container_name: get_my
    image: bmcgonag/get_my:latest
    ports:
      - "80:3000"    # you can change the left side to a less common port number if needed
    healthcheck:
      test: curl --fail -s http://localhost:3000/ || exit 1
      interval: 30s
      timeout: 10s
      retries: 3
    links:
      - mongo
    depends_on:
      - mongo
    restart: unless-stopped
  mongo:
    container_name: get_my-mongo
    image: mongo
    volumes:
      - ./data:/data/db    # this will store the application data in the same directory where you have the docker-compose.yml file. the directory on your host will be called 'data'.
    restart: unless-stopped
```

### On Host using NodeJS
1. You need to have a mongo database instance installed and ready to use. You also need to know the port it uses, as well as create a database in mongo for the 'Get My' application. I recommend naming the database 'get-my'.
2. You need to have NodeJS installed (the hard part is that this application is build in MeteorJS, and currently uses a node version of 14.21.4 - which you'll find isn't available for download from node themselves anymore).  You need 14.21.4 and can get a tar.gz file of it here: https://guide.meteor.com/using-node-v14.21.4
3. Once setup. you need to set some environment variables
  - `ROOT_URL=`(whatever you want your node application to accessed at, e.g. `http://localhost`, etc)
  - `MONGO_URL=`(ip and port for your mongodb in this format `mongodb://localhost:27017/get-my`)  This is the url for the application to reach the mongo database, and does not have to exist on the same machine, but must be reachable by IP or hostname, and port. The 'get-my' in this url is the database name you setup.
  - `PORT=3000`
  - `MAIL_URL=` not a working feature yet, but if you want smtp email to work you'd need to set this to be something like `smtp://USERNAME:PASSWORD@HOST:PORT`
4. Download the source bundle (I'll add a url to release bundle soon)
5. unzip the source bundle
5. Move into the bundle/program/server folder, and run `npm install`. This should install all needed dependancies. 
6. move back to the bundle folder, and run `node main.js`.  This will start the application running.
7. There are multiple node applications to help keep a node js app running in the background, like PM2, etc...but I'll leave that up to you to get going. 

## Documentation
The documentation for Get My is a work in progress. Please bare with me until I am able to get it done. I'll work on it as I have time.

You can find the [documentation here](https://getmydocs.opensourceisawesome.com).

The documentation is being done in the amazing open source software [Bookstack](https://www.bookstackapp.com/).

If you'd like to contribute by writing some documentation, please contact me me with an, and I'll see about getting you access to contribute.

I have a video introduction on my YouTube channel here as well: https://youtu.be/hbGNTzU24hI?si=K5mcb8FuzSWNCueB

## Keeping it Simple
- Registration / Login System built in
- 1st user to register is the system admin by default.
- All other useers are added as "user".
- No serious permissions difference as of now other than system settings and user management for the system admin.
- Product Information Levels:
  - Products
    - Product Name
    - Store you most frequently buy it at (optional)
- Lists
  - Can be shared with other users to add items (green text)
  - Can be private for keeping your own list of things to buy or tasks to do.
  - Can be marked as complete and removed from view.
- List Items
  - Added to specific lists
  - Can be marked as
    - Ordered (crossed out on list, but visible)
    - Received (check mark icon to remove from visible list, but can be seen by selecting an option at the top of the list if needed.)
- Tasks
  - A simple todo list of tasks that can be made for yourself, or if you're an admin, for others on the system, and a due date can be assigned.  No reminders, or push notifications, just open it, and look at your list of todos, then mark it complete when you finish one.
    - Add Todo Items
      - Add Due Date
    - Mark Items as Completed
    - See Overdue items in a different color (need an icond for this too)
- Menus
  - Create a Menu (we like to create one for a week or so, but you can create different Menus at the same time)
  - Add menu items to a simple menu list. 
    - Name a Menu (e.g. Guisado, Beans, Rice, side-salad)
    - Pick a date to serve the item.
  - Dashboard shows the item for that day. 
  - Shift the menu by 1 day with a simple click (we use this when we end up eating something else in the middle of menu week.)
- User Config
  - Dark Mode Preference
- System Config
  - Allow / Disallow Registration
  - Allow / Disallow System Admin registration

## What Get My Is
- a Simple Lists App
## What Get My Is Not
- a Reminders app with calendar / timer events.
- A full ERP / CRM for your home.
- A portal or interface to any shopping / eCommerce sit online.
## Built With
- [MeteorJS](https://www.meteor.com/)
  - Blaze UI
- [MongoDB](https://www.mongodb.com/)
- [Materialize-css](https://materializeweb.com/)

## License
MIT
Copyright 2022 - Brian McGonagill

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
