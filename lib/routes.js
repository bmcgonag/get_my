FlowRouter.route('/dashboard', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNoRoute',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNotLoggedIn',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/reg', {
    name: 'reg',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "reg" });
    }
});

FlowRouter.route('/userMgmt', {
    name: 'userMgmt',
    action() {
        BlazeLayout.render('MainLayout', { main: 'userMgmt' });
    }
});

FlowRouter.route('/manageStore', {
    name: 'storeMgmt',
    action() {
        BlazeLayout.render('MainLayout', { main: 'storeMgmt' });
    }
});

FlowRouter.route('/manage', {
    name: 'mgmtPage',
    action() {
        BlazeLayout.render('MainLayout', { main: 'mgmtPage' });
    }
});

FlowRouter.route('/manageProduct', {
    name: 'manageProduct',
    action() {
        BlazeLayout.render('MainLayout', { main: 'prodMgmt' });
    }
});

FlowRouter.route('/manageLists', {
    name: 'manageLists',
    action() {
        BlazeLayout.render('MainLayout', { main: 'listMgmt' });
    }
});

FlowRouter.route('/mylists', {
    name: 'mylists',
    action() {
        BlazeLayout.render('MainLayout', { main: 'listsMain' });
    }
});

FlowRouter.route('/listItems', {
    name: 'listItems',
    action() {
        BlazeLayout.render('MainLayout', { main: 'listItemsMain' });
    }
});

FlowRouter.route('/mymenus', {
    name: 'mymenus',
    action() {
        BlazeLayout.render('MainLayout', { main: 'mainMenu' });
    }
});

FlowRouter.route('/menuItems', {
    name: 'menuItems',
    action() {
        BlazeLayout.render('MainLayout', { main: 'menuItems' });
    }
});

FlowRouter.route('/taskHome', {
    name: 'taskHome',
    action() {
        BlazeLayout.render('MainLayout', { main: 'taskHome' });
    }
});

FlowRouter.route('/myTasks', {
    name: 'myTasks',
    action() {
        BlazeLayout.render('MainLayout', { main: 'myTasks' });
    }
});

FlowRouter.route('/systemAdmin', {
    name: 'systemAdmin',
    action() {
        BlazeLayout.render('MainLayout', { main: 'systemAdmin' });
    }
});

FlowRouter.route('/cleanUp', {
    name: 'cleanUp',
    action() {
        BlazeLayout.render('MainLayout', { main: 'cleanUp'});
    }
});

FlowRouter.route('/mySettings', {
    name: 'mySettings',
    action() {
        BlazeLayout.render('MainLayout', { main: 'userConfig'});
    }
});